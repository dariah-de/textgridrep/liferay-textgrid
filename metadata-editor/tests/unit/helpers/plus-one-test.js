import { plusOne } from '../../../helpers/plus-one';
import { module, test } from 'qunit';

module('Unit | Helper | plus one');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = plusOne(1);
  assert.equal(result, 2);
});
