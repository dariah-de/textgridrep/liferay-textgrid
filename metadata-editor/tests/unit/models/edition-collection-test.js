import { moduleForModel, test } from 'ember-qunit';

moduleForModel('edition-collection', 'Unit | Model | edition collection', {
  // Specify the other units that are required for this test.
  needs: ['model:edition']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
