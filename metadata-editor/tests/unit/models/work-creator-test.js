import { moduleForModel, test } from 'ember-qunit';

moduleForModel('work-creator', 'Unit | Model | work creator', {
  // Specify the other units that are required for this test.
  needs: ['model:work']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
