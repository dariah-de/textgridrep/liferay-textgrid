import { moduleForModel, test } from 'ember-qunit';

moduleForModel('edition', 'Unit | Model | edition', {
  // Specify the other units that are required for this test.
  needs: [
    'model:edition-collection',
    'model:edition-contributor',
    'model:edition-editorial-identifier',
    'model:edition-item',
    'model:edition-script',
    'model:edition-source',
    'model:edition-title',
  ]
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
