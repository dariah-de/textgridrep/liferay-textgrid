import { moduleForModel, test } from 'ember-qunit';

moduleForModel('item-rights-holder', 'Unit | Model | item rights holder', {
  // Specify the other units that are required for this test.
  needs: ['model:item']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
