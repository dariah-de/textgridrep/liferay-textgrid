import { moduleForModel, test } from 'ember-qunit';

moduleForModel('edition-title', 'Unit | Model | edition title', {
  // Specify the other units that are required for this test.
  needs: ['model:edition']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
