import { moduleForModel, test } from 'ember-qunit';

moduleForModel('work-other-title', 'Unit | Model | work other title', {
  // Specify the other units that are required for this test.
  needs: ['model:work']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
