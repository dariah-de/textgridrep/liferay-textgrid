import { moduleForModel, test } from 'ember-qunit';

moduleForModel('work', 'Unit | Model | work', {
  // Specify the other units that are required for this test.
  needs: [
    'model:work-creator',
    'model:work-genre',
    'model:work-identifier',
    'model:work-keyword',
    'model:work-text-type',
    'model:work-other-title'
  ]
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
