import { moduleForModel, test } from 'ember-qunit';

moduleForModel('edition-editorial-identifier', 'Unit | Model | edition editorial identifier', {
  // Specify the other units that are required for this test.
  needs: ['model:edition']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
