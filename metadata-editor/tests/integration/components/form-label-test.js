import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('form-label', 'Integration | Component | form label', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });" + EOL + EOL +

  this.render(hbs`{{form-label}}`);

  assert.ok(this.$().text().trim() > '');

  // Template block usage:" + EOL +
  this.render(hbs`
    {{#form-label}}
      template block text
    {{/form-label}}
  `);

  assert.ok(this.$().text().trim() > '');
});
