import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('add-remove-buttons', 'Integration | Component | add remove buttons', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });" + EOL + EOL +

  this.render(hbs`{{add-remove-buttons}}`);

  assert.ok(this.$().text().trim() > '');

  // Template block usage:" + EOL +
  this.render(hbs`
    {{#add-remove-buttons}}
      template block text
    {{/add-remove-buttons}}
  `);

  assert.ok(this.$().text().trim() > '');
});
