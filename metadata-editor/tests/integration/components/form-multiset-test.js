import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('form-multiset', 'Integration | Component | form multiset', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });" + EOL + EOL +

  this.render(hbs`{{form-multiset}}`);

  assert.ok(this.$().text().trim() > '');

  // Template block usage:" + EOL +
  this.render(hbs`
    {{#form-multiset}}
      template block text
    {{/form-multiset}}
  `);

  assert.ok(this.$().text().trim() > '');
});
