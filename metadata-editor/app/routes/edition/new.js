import Ember from 'ember';
import Editable from '../../mixins/editable';

export default Ember.Route.extend(Editable, {
  model() {
    return this.new('edition');
  },
  renderTemplate: function() {
    this.render('edition.edit');
  }
});
