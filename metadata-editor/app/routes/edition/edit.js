import Ember from 'ember';
import Editable from '../../mixins/editable';

export default Ember.Route.extend(Editable, {
  model(params) {
    return this.store.findRecord('edition', params.edition_id, { reload: true });
  }
});
