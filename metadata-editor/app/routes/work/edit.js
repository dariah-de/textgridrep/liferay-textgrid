import Ember from 'ember';
import Editable from '../../mixins/editable';

export default Ember.Route.extend(Editable, {
  model(params) {
    return this.store.findRecord('work', params.work_id, { reload: true });
  },
  actions: {
    toggleCreationDate() {
      this.currentModel.set('creationDateIsRange', ! this.currentModel.get('creationDateIsRange'));
    }
  }
});
