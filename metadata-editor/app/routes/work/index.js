import Ember from 'ember';
import Editable from '../../mixins/editable';

export default Ember.Route.extend(Editable, {
  model() {
    return this.store.findAll('work');
  }
});
