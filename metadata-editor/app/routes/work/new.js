import Ember from 'ember';
import Editable from '../../mixins/editable';

export default Ember.Route.extend(Editable, {
  model() {
    return this.new('work');
  },
  renderTemplate: function() {
    this.render('work.edit');
  },
  actions: {
    toggleCreationDate() {
      this.currentModel.set('creationDateIsRange', ! this.currentModel.get('creationDateIsRange'));
    }
  }
});
