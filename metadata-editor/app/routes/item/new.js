import Ember from 'ember';
import Editable from '../../mixins/editable';

export default Ember.Route.extend(Editable, {
  model() {
    return this.new('item');
  },
  actions: {
    createDummyItem() {
      var model = this.currentModel;

      model.set('size', Math.round( Math.random() * 1e7 ));

      var date = new Date();
      model.set('created', date.toISOString());

      this.send('save');
    }
  }
});
