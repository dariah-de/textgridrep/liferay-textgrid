import Ember from 'ember';
import config from '../config/environment';
import {flashMessage} from '../helpers/flash-message';

export default Ember.Mixin.create({
  controller: null,
  i18n: Ember.inject.service(),
  actions: {
    delete(record) {
      // TODO: Delete orphaned child records too
      record.destroyRecord();
    },
    revert() {
      // Reload everything, discard changes
      // TODO: Replace sledgehammer approach with something more elegant
      window.location.href = window.location.href;
    },
    save() {
      var promises = [];
      this.currentModel.save().then( (record) => {
        record.eachRelationship( (name, descriptor) => {
          if ( descriptor.kind === 'hasMany' ) {
            var relatedRecords = this.store.peekAll(descriptor.type);
            relatedRecords.forEach( (relatedRecord) => {
              // TODO: Check for duplicates
              if ( relatedRecord.get('shouldDelete') ) {
                relatedRecord.deleteRecord();
              }
              promises.push(relatedRecord.save());
            });
          }
        });
        Ember.RSVP.all(promises).then( () => {
          flashMessage('saved');
          this.set('controller.isDirty', false);
          this.cleanUp();
          var editRoute = this.get('controller.type') + '.edit';
          this.transitionTo(editRoute, this.currentModel.id);
          return true;
        }, () => {
          flashMessage('error', true);
          return false;
        });
      });
    },
    willTransition(transition) {
      var controller = this.get('controller');
      var message = this.get('i18n').t('discardChangesPrompt');
      if ( controller.get('isDirty') && ! confirm(message) ) {
        transition.abort();
      } else {
        this.store.unloadAll();
        this.cleanUp();
        this.controllerFor('application').set('loading', true);
        flashMessage(false);
      }
    },
  },
  activate() {
    this._super();
    this.controllerFor('application').set('loading', false);
    window.scrollTo(0, 0);
    window.onbeforeunload = () => {
      if ( this.get('controller.isDirty') ) {
        return this.get('i18n').t('discardChangesPrompt');
      }
    };
  },
  afterModel(model) {
    var immutables = model.get('immutables');
    if ( immutables ) {
      this.loadImmutables(immutables);
    }
    this.trackChanges();
  },
  becameDirty() {
    this.set('controller.isDirty', true);
    flashMessage('notSaved', true);
  },
  cleanUp() {
    var $ = Ember.$;
    $(':input').removeClass('is-dirty');
    this.set('controller.isDirty', false);
  },
  loadImmutables(immutables) {
    var promises = [];

    for ( let immutable in immutables ) {
      let source = immutables[immutable];
      let apiPath = `${config.apiHost}/${config.apiNamespace}/${source}`;
      promises.push(Ember.$.getJSON(apiPath));
    }

    Ember.RSVP.all(promises).then( (jsonArray) => {
      for (let index in jsonArray) {
        if (index === '_super') {
          break;
        }

        let data = {};
        let immutable = Object.keys(immutables)[index];
        let json = jsonArray[index];
        let source = immutables[immutable];

        if ( json[source] && typeof json[source][0] === 'object' ) {
          // Data is an array of objects, not a flat object
          for ( let object of json[source] ) {
            let objectType = immutable.charAt(0).toUpperCase() + immutable.substr(1).replace(/s$/, '');
            data[object.id] = `${objectType} ${object.id}`;
            if ( immutable === 'items' ) {
              data[object.id] += `${object.mimeType}, ${Math.round(object.size / 1000)} KB`;
            } else if ( immutable === 'editions' ) {
              // TODO
            } else if ( immutable === 'works' && object.uniformTitle ) {
              data[object.id] += `, ${object.uniformTitle}`;
            }
            data[object.id] += `, zuletzt geändert ${object.lastModified}`;
          }
        } else {
          data = json[source];
        }

        this.get('controller').set(immutable, data);
      }
    });
  },
  new(type) {
    var model = this.store.createRecord(type);
    model.eachRelationship( (name, descriptor) => {
      if (descriptor.kind === 'hasMany' && descriptor.options.required) {
        let record = this.store.createRecord(descriptor.type);
        model.get(descriptor.key).pushObject(record);
      }
    });
    return model;
  },
  setupController(controller, model) {
    this._super(controller, model);
    var baseRouteName = this.get('routeName').replace(/\..*/, '');
    controller.set('type', baseRouteName);
    this.set('controller', controller);
  },
  trackChanges: function() {
    var $ = Ember.$;
    var self = this;
    // NOTE: This breaks databinding on checkboxes, which we don't use anyway
    $('body').off('change').on('change', ':input', function() {
      $(this).addClass('is-dirty');
      self.becameDirty();
    });
  }
});
