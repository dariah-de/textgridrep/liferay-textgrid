import Ember from 'ember';

const AddRemoveButtons = Ember.Component.extend({
  actions: {
    add() {
      var typeName = this.get('parent.content.type.modelName');
      var record = this.get('store').createRecord(typeName);
      this.get('parent').insertAt(this.get('index') + 1, record);
    },
    remove() {
      var record = this.get('parent').objectAt(this.get('index'));
      record.set('shouldDelete', true);
      this.get('parent').removeAt(this.get('index'));
      // TODO: Hack to trigger becameDirty on route
      var hiddenInput = Ember.$('<input/>').hide();
      hiddenInput.appendTo('body').change().remove();
    }
  },
  required: Ember.computed( 'parent', 'index', function() {
    return this.get('parent.content.length') === 1 && this.get('parent.content.relationship.relationshipMeta.options.required'); // lol
  })
});

AddRemoveButtons.reopenClass({
  positionalParams: ['store', 'index', 'parent']
});

export default AddRemoveButtons;
