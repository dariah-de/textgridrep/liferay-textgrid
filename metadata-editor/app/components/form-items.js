import Ember from 'ember';

const FormItem = Ember.Component.extend({
  store: Ember.inject.service(),
  tagName: ''
});

FormItem.reopenClass({
  positionalParams: ['records']
});

export default FormItem;
