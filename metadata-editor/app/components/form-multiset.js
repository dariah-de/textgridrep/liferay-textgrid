import Ember from 'ember';

const FormMultiSet = Ember.Component.extend({
  store: Ember.inject.service(),
  hasOptions: true,
  tagName: '',
  buttonLabel: Ember.computed('name', function() {
    // Poor man's type name extract and singularize
    return this.get('name') ? this.get('name').replace(/s$/, '') : '';
  })
});

FormMultiSet.reopenClass({
  positionalParams: ['name', 'records']
});

export default FormMultiSet;
