import Ember from "ember";

export default Ember.Component.extend({
  classNameBindings: ['label:form-group'],
  i18n: Ember.inject.service(),
  select: null,
  didInsertElement() {
    this.set('select', this.$().find('select:first'));
    this.initSelect2();
  },
  initSelect2: function() {
    if ( this.get('options') ) {
      Ember.run.scheduleOnce('afterRender', this, function() {
        this.get('select').select2({
          theme: 'bootstrap',
          language: {
            noResults: function() {
              return this.get('i18n').t('noResults');
            }
          }
        });
      });
    }
  }.observes('options', 'value'),
  actions: {
    select(value) {
      this.set('value', value);
    }
  }
});
