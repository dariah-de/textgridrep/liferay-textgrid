import Ember from 'ember';

export default Ember.Component.extend({
  classNameBindings: ['label:form-group']
});
