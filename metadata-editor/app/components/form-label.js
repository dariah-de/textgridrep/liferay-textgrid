import Ember from 'ember';

const FormLabel = Ember.Component.extend({});

FormLabel.reopenClass({
  positionalParams: ['name']
});

export default FormLabel;
