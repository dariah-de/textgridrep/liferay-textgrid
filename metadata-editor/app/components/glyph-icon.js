import Ember from 'ember';

const GlyphIcon = Ember.Component.extend({
  tagName: ''
});

GlyphIcon.reopenClass({
  positionalParams: ['name']
});

export default GlyphIcon;
