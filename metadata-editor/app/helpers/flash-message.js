import Ember from 'ember';

export function flashMessage(id, stayOpen) {
  var $ = Ember.$;
  $('.flash-message').hide();

  if ( id !== false ) {
    var $message = $('#' + id);
    $message.show();
    if ( ! stayOpen ) {
      setTimeout( function() {
        $message.animate({opacity: 0}, 4000, function() {
          $message.hide().css({opacity: 1});
        });
      }, 2000);
    }
  }

  return true;
}

export default Ember.Helper.helper(flashMessage);
