import DS from 'ember-data';

export default DS.Model.extend({
  edition: DS.belongsTo('edition'),
  scriptId: DS.attr()
});
