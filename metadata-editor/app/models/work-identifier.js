import DS from 'ember-data';

export default DS.Model.extend({
  work: DS.belongsTo('work'),
  value: DS.attr(),
  type: DS.attr()
});
