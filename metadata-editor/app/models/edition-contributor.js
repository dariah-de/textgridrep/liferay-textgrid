import DS from 'ember-data';

export default DS.Model.extend({
  edition: DS.belongsTo('edition'),
  type: DS.attr(),
  name: DS.attr(),
  givenName: DS.attr(),
  familyName: DS.attr(),
  role: DS.attr(),
  identifier: DS.attr()
});
