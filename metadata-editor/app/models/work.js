import DS from 'ember-data';

export default DS.Model.extend({
  abstract: DS.attr(),
  creators: DS.hasMany('workCreator', {required: true}),
  creationDate: DS.attr({required: true}),
  creationDateNotBefore: DS.attr(),
  creationDateNotAfter: DS.attr(),
  creationDateApprox: DS.attr(),
  creationDateNotBeforeApprox: DS.attr(),
  creationDateNotAfterApprox: DS.attr(),
  creationDateIsRange: DS.attr('boolean'),
  identifiers: DS.hasMany('workIdentifier'),
  textgridUri: DS.attr({required: true}),
  keywords: DS.hasMany('workKeyword'),
  uniformTitle: DS.attr({required: true}),
  otherTitles: DS.hasMany('workOtherTitle'),
  lastModified: DS.attr(),
  genres: DS.hasMany('workGenre'),
  textTypes: DS.hasMany('workTextType', {required: true}),
  immutables: function() {
    return {
      keywordTypes: 'keyword-types'
    };
  }.property()
});
