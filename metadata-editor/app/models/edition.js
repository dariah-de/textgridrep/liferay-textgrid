import DS from 'ember-data';

export default DS.Model.extend({
  items: DS.hasMany('editionItems'),
  note: DS.attr(),
  scripts: DS.hasMany('editionScript'),
  editorialIdentifiers: DS.hasMany('editionEditorialIdentifier'),
  textgridUri: DS.attr({required: true}),
  issuanceDate: DS.attr({required: true}),
  language: DS.attr(),
  work: DS.attr(),
  license: DS.attr({required: true}),
  sources: DS.hasMany('editionSource'),
  titles: DS.hasMany('editionTitle', {required: true}),
  collections: DS.hasMany('editionCollection'),
  contributors: DS.hasMany('editionContributor', {required: true}),
  lastModified: DS.attr(),
  immutables: function() {
    return {
      languages: 'iso-639-3-short',
      scripts: 'iso-15924',
      items: 'items',
      contributorTypes: 'contributor-types',
      contributorRoles: 'contributor-roles',
      works: 'works'
    };
  }.property()
});
