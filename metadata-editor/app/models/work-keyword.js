import DS from 'ember-data';

export default DS.Model.extend({
  work: DS.belongsTo('work'),
  type: DS.attr(), // place, subject or time
  value: DS.attr()
});
