import DS from 'ember-data';

export default DS.Model.extend({
  created: DS.attr({required: true}),
  size: DS.attr('number', {required: true}),
  mimeType: DS.attr(),
  identifiers: DS.hasMany('itemIdentifier'),
  textgridUri: DS.attr({required: true}),
  lastModified: DS.attr({required: true}),
  edition: DS.attr(),
  contributor: DS.attr({required: true}),
  rightsHolders: DS.hasMany('itemRightsHolder', {required: true}),
  objectNames: DS.hasMany('itemObjectName', {required: true}),
  sizeKB: function() {
    return Math.round(this.get('size') / 100) / 10;
  }.property('size'),
  sizeMB: function() {
    return Math.round(this.get('size') / 100000) / 10;
  }.property('size'),
  immutables: function() {
    return {
      mimeTypes: 'mime-types',
      editions: 'editions'
    };
  }.property()
});
