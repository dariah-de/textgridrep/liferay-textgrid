import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('item', function() {
    this.route('new');
    this.route('edit', { path: 'edit/:item_id' } );
  });
  this.route('edition', function() {
    this.route('new');
    this.route('edit', { path: 'edit/:edition_id' } );
  });
  this.route('work', function() {
    this.route('new');
    this.route('edit', { path: 'edit/:work_id' } );
  });
});

export default Router;
