# TextGrid Vector Icons

All icons were designed on a 16×16 pixel grid and are best used in integral multiples of this size. Icons do not have fill styles, so they default to black.

Only minified SVG files should be committed to this repository. Make sure to remove all comments and redundant white-space, or use [SVGmin](https://code.google.com/p/svgmin/) (also available for [Gulp](https://www.npmjs.com/package/gulp-svgmin "SVGmin for Gulp") and [Grunt](https://github.com/sindresorhus/grunt-svgmin "SVGmin for Grunt")).
