jQuery.fn.extend({
	renumberInputs: function() {
		return this.each(function( index ) {
			$(this).find(':input').each(function() {
				this.name = this.name.replace(/\[\d+\]/, '[' + index + ']')
			})
		})
	}
})
