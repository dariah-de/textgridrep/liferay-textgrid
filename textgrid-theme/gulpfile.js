var gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	concat = require('gulp-concat'),
	notify = require('gulp-notify'),
	sass = require('gulp-sass'),
	sassGlob = require('gulp-sass-glob'),
	svgmin = require('gulp-svgmin'),
	uglify = require('gulp-uglify')

var paths = {
	styleSrc: ['sass/**/*.sass', '!sass/vendor/**/*.{sass,scss}'],
	styleDest: 'src/main/webapp/css',
	scriptSrc: ['js/licenses/**/*.js', 'js/**/*.js'],
	scriptDest: 'src/main/webapp/js',
	imgSrc: 'src/main/webapp/images/*.svg',
	imgDest: 'src/main/webapp/images'
}

gulp.task('script', function() {
	gulp.src(paths.scriptSrc)
		.pipe(uglify({
			preserveComments: 'some'
		}))
		.pipe(concat('theme.js'))
		.pipe(gulp.dest(paths.scriptDest))
})

gulp.task('style', function() {
	gulp.src(paths.styleSrc)
		.pipe(sassGlob())
		.pipe(sass({
			indentedSyntax: true,
			outputStyle: 'compressed'
		}))
		.on( 'error', notify.onError() )
		.pipe(autoprefixer())
		.pipe(gulp.dest(paths.styleDest))
})

gulp.task('img', function() {
	gulp.src(paths.imgSrc)
		.pipe(svgmin())
		.pipe(gulp.dest(paths.imgDest))
})

gulp.task('watch', ['script', 'style'], function() {
	gulp.watch(paths.scriptSrc, ['script'])
	gulp.watch(paths.styleSrc, ['style'])
})

gulp.task('default', ['script', 'style'])
