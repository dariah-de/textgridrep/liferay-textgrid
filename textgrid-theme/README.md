# Liferay 6 Theme for textgrid.de

## Compiling and installation

### Compile CSS

CSS is compiled from Sass via [gulp](http://gulpjs.com/). The theme comes with
compiled CSS, so compiling is only required after changes have been made in
Sass.

1. Install gulp globally: `npm install -g gulp` (NPM is likely already installed
   with [Node.js](https://nodejs.org/))
2. Install dev dependencies: `npm install`
3. Compile Sass to CSS: `gulp` (use `gulp watch` to continuously watch for
   changes)
4. Rebuild theme package: `mvn package`

### Install theme manually
In Liferay, go to _Admin &rarr; Site Administration &rarr; Pages | Available
Themes &rarr; Install More | App Manager &rarr; Install_. Select the WAR theme
file and click _Install_.

## Theme settings

For every page, the following options can be set under _Edit Page &rarr; Look
and Feel &rarr; Settings_:
- Show breadcrumbs (default: `true`):
  Toggle breadcrumbs display
- Highlight sidebar (default: `false`):
  Toggle sidebar background. Note that this also changes the padding for all columns.
- TextGrid Repository (default: `false`):
  Use TextGridRep styles and replace the logo. Also replaces search with shelf link.
- TextGrid Repository search in header (default: `false`):
  Display the TextGrid Repository search in the header (always visible).

## Extending the theme (aka adding styles)

Styles are written in Sass (indented syntax), using a variant of the
[BEM](https://en.bem.info/method/) naming scheme for class names. In BEM -
meaning block, element, modifier - all styles belong to one of these types:
- `.block` represents the higher level of an abstraction or component
- `.block_element`, divided by `_`, represents a descendent of .block. Use only
  one level of descendency, it does not have to correspond with the actual DOM.
- `.-modifier`, prepended by `-` (think command-line switch), represents a
  different state of a .block or .block_element and can only be used
  alongside those.

In Sass, this looks like `.namespace.block-name_element-name &.-modifier-name.`
Namespace is added to improve specifity and thus override default styles. This
corresponds to
`<element class="namespace block-name_element-name -modifier-name"`> in
HTML/Velocity. Up to two class-less child elements are fine if otherwise extra
overrides would be necessary. Use !important only for base theme overrides
as a last resort.
