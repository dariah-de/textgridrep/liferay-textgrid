package info.textgrid.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Basket service. Represents a row in the &quot;textgridrep_Basket&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see BasketModel
 * @see info.textgrid.model.impl.BasketImpl
 * @see info.textgrid.model.impl.BasketModelImpl
 * @generated
 */
public interface Basket extends BasketModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link info.textgrid.model.impl.BasketImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
