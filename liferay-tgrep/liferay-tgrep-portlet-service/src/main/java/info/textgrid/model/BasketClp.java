package info.textgrid.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.textgrid.service.BasketLocalServiceUtil;
import info.textgrid.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class BasketClp extends BaseModelImpl<Basket> implements Basket {
    private String _uuid;
    private long _basketId;
    private long _userId;
    private String _userUuid;
    private String _textgridUri;
    private int _order;
    private BaseModel<?> _basketRemoteModel;
    private Class<?> _clpSerializerClass = info.textgrid.service.ClpSerializer.class;

    public BasketClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Basket.class;
    }

    @Override
    public String getModelClassName() {
        return Basket.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _basketId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setBasketId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _basketId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("uuid", getUuid());
        attributes.put("basketId", getBasketId());
        attributes.put("userId", getUserId());
        attributes.put("textgridUri", getTextgridUri());
        attributes.put("order", getOrder());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String uuid = (String) attributes.get("uuid");

        if (uuid != null) {
            setUuid(uuid);
        }

        Long basketId = (Long) attributes.get("basketId");

        if (basketId != null) {
            setBasketId(basketId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        String textgridUri = (String) attributes.get("textgridUri");

        if (textgridUri != null) {
            setTextgridUri(textgridUri);
        }

        Integer order = (Integer) attributes.get("order");

        if (order != null) {
            setOrder(order);
        }
    }

    @Override
    public String getUuid() {
        return _uuid;
    }

    @Override
    public void setUuid(String uuid) {
        _uuid = uuid;

        if (_basketRemoteModel != null) {
            try {
                Class<?> clazz = _basketRemoteModel.getClass();

                Method method = clazz.getMethod("setUuid", String.class);

                method.invoke(_basketRemoteModel, uuid);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getBasketId() {
        return _basketId;
    }

    @Override
    public void setBasketId(long basketId) {
        _basketId = basketId;

        if (_basketRemoteModel != null) {
            try {
                Class<?> clazz = _basketRemoteModel.getClass();

                Method method = clazz.getMethod("setBasketId", long.class);

                method.invoke(_basketRemoteModel, basketId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getUserId() {
        return _userId;
    }

    @Override
    public void setUserId(long userId) {
        _userId = userId;

        if (_basketRemoteModel != null) {
            try {
                Class<?> clazz = _basketRemoteModel.getClass();

                Method method = clazz.getMethod("setUserId", long.class);

                method.invoke(_basketRemoteModel, userId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
    }

    @Override
    public void setUserUuid(String userUuid) {
        _userUuid = userUuid;
    }

    @Override
    public String getTextgridUri() {
        return _textgridUri;
    }

    @Override
    public void setTextgridUri(String textgridUri) {
        _textgridUri = textgridUri;

        if (_basketRemoteModel != null) {
            try {
                Class<?> clazz = _basketRemoteModel.getClass();

                Method method = clazz.getMethod("setTextgridUri", String.class);

                method.invoke(_basketRemoteModel, textgridUri);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getOrder() {
        return _order;
    }

    @Override
    public void setOrder(int order) {
        _order = order;

        if (_basketRemoteModel != null) {
            try {
                Class<?> clazz = _basketRemoteModel.getClass();

                Method method = clazz.getMethod("setOrder", int.class);

                method.invoke(_basketRemoteModel, order);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getBasketRemoteModel() {
        return _basketRemoteModel;
    }

    public void setBasketRemoteModel(BaseModel<?> basketRemoteModel) {
        _basketRemoteModel = basketRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _basketRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_basketRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            BasketLocalServiceUtil.addBasket(this);
        } else {
            BasketLocalServiceUtil.updateBasket(this);
        }
    }

    @Override
    public Basket toEscapedModel() {
        return (Basket) ProxyUtil.newProxyInstance(Basket.class.getClassLoader(),
            new Class[] { Basket.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        BasketClp clone = new BasketClp();

        clone.setUuid(getUuid());
        clone.setBasketId(getBasketId());
        clone.setUserId(getUserId());
        clone.setTextgridUri(getTextgridUri());
        clone.setOrder(getOrder());

        return clone;
    }

    @Override
    public int compareTo(Basket basket) {
        int value = 0;

        if (getOrder() < basket.getOrder()) {
            value = -1;
        } else if (getOrder() > basket.getOrder()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof BasketClp)) {
            return false;
        }

        BasketClp basket = (BasketClp) obj;

        long primaryKey = basket.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{uuid=");
        sb.append(getUuid());
        sb.append(", basketId=");
        sb.append(getBasketId());
        sb.append(", userId=");
        sb.append(getUserId());
        sb.append(", textgridUri=");
        sb.append(getTextgridUri());
        sb.append(", order=");
        sb.append(getOrder());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(19);

        sb.append("<model><model-name>");
        sb.append("info.textgrid.model.Basket");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>uuid</column-name><column-value><![CDATA[");
        sb.append(getUuid());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>basketId</column-name><column-value><![CDATA[");
        sb.append(getBasketId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userId</column-name><column-value><![CDATA[");
        sb.append(getUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>textgridUri</column-name><column-value><![CDATA[");
        sb.append(getTextgridUri());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>order</column-name><column-value><![CDATA[");
        sb.append(getOrder());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
