package info.textgrid.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class BasketSoap implements Serializable {
    private String _uuid;
    private long _basketId;
    private long _userId;
    private String _textgridUri;
    private int _order;

    public BasketSoap() {
    }

    public static BasketSoap toSoapModel(Basket model) {
        BasketSoap soapModel = new BasketSoap();

        soapModel.setUuid(model.getUuid());
        soapModel.setBasketId(model.getBasketId());
        soapModel.setUserId(model.getUserId());
        soapModel.setTextgridUri(model.getTextgridUri());
        soapModel.setOrder(model.getOrder());

        return soapModel;
    }

    public static BasketSoap[] toSoapModels(Basket[] models) {
        BasketSoap[] soapModels = new BasketSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static BasketSoap[][] toSoapModels(Basket[][] models) {
        BasketSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new BasketSoap[models.length][models[0].length];
        } else {
            soapModels = new BasketSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static BasketSoap[] toSoapModels(List<Basket> models) {
        List<BasketSoap> soapModels = new ArrayList<BasketSoap>(models.size());

        for (Basket model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new BasketSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _basketId;
    }

    public void setPrimaryKey(long pk) {
        setBasketId(pk);
    }

    public String getUuid() {
        return _uuid;
    }

    public void setUuid(String uuid) {
        _uuid = uuid;
    }

    public long getBasketId() {
        return _basketId;
    }

    public void setBasketId(long basketId) {
        _basketId = basketId;
    }

    public long getUserId() {
        return _userId;
    }

    public void setUserId(long userId) {
        _userId = userId;
    }

    public String getTextgridUri() {
        return _textgridUri;
    }

    public void setTextgridUri(String textgridUri) {
        _textgridUri = textgridUri;
    }

    public int getOrder() {
        return _order;
    }

    public void setOrder(int order) {
        _order = order;
    }
}
