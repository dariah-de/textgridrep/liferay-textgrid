package info.textgrid.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Basket}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Basket
 * @generated
 */
public class BasketWrapper implements Basket, ModelWrapper<Basket> {
    private Basket _basket;

    public BasketWrapper(Basket basket) {
        _basket = basket;
    }

    @Override
    public Class<?> getModelClass() {
        return Basket.class;
    }

    @Override
    public String getModelClassName() {
        return Basket.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("uuid", getUuid());
        attributes.put("basketId", getBasketId());
        attributes.put("userId", getUserId());
        attributes.put("textgridUri", getTextgridUri());
        attributes.put("order", getOrder());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String uuid = (String) attributes.get("uuid");

        if (uuid != null) {
            setUuid(uuid);
        }

        Long basketId = (Long) attributes.get("basketId");

        if (basketId != null) {
            setBasketId(basketId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        String textgridUri = (String) attributes.get("textgridUri");

        if (textgridUri != null) {
            setTextgridUri(textgridUri);
        }

        Integer order = (Integer) attributes.get("order");

        if (order != null) {
            setOrder(order);
        }
    }

    /**
    * Returns the primary key of this basket.
    *
    * @return the primary key of this basket
    */
    @Override
    public long getPrimaryKey() {
        return _basket.getPrimaryKey();
    }

    /**
    * Sets the primary key of this basket.
    *
    * @param primaryKey the primary key of this basket
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _basket.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the uuid of this basket.
    *
    * @return the uuid of this basket
    */
    @Override
    public java.lang.String getUuid() {
        return _basket.getUuid();
    }

    /**
    * Sets the uuid of this basket.
    *
    * @param uuid the uuid of this basket
    */
    @Override
    public void setUuid(java.lang.String uuid) {
        _basket.setUuid(uuid);
    }

    /**
    * Returns the basket ID of this basket.
    *
    * @return the basket ID of this basket
    */
    @Override
    public long getBasketId() {
        return _basket.getBasketId();
    }

    /**
    * Sets the basket ID of this basket.
    *
    * @param basketId the basket ID of this basket
    */
    @Override
    public void setBasketId(long basketId) {
        _basket.setBasketId(basketId);
    }

    /**
    * Returns the user ID of this basket.
    *
    * @return the user ID of this basket
    */
    @Override
    public long getUserId() {
        return _basket.getUserId();
    }

    /**
    * Sets the user ID of this basket.
    *
    * @param userId the user ID of this basket
    */
    @Override
    public void setUserId(long userId) {
        _basket.setUserId(userId);
    }

    /**
    * Returns the user uuid of this basket.
    *
    * @return the user uuid of this basket
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.lang.String getUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _basket.getUserUuid();
    }

    /**
    * Sets the user uuid of this basket.
    *
    * @param userUuid the user uuid of this basket
    */
    @Override
    public void setUserUuid(java.lang.String userUuid) {
        _basket.setUserUuid(userUuid);
    }

    /**
    * Returns the textgrid uri of this basket.
    *
    * @return the textgrid uri of this basket
    */
    @Override
    public java.lang.String getTextgridUri() {
        return _basket.getTextgridUri();
    }

    /**
    * Sets the textgrid uri of this basket.
    *
    * @param textgridUri the textgrid uri of this basket
    */
    @Override
    public void setTextgridUri(java.lang.String textgridUri) {
        _basket.setTextgridUri(textgridUri);
    }

    /**
    * Returns the order of this basket.
    *
    * @return the order of this basket
    */
    @Override
    public int getOrder() {
        return _basket.getOrder();
    }

    /**
    * Sets the order of this basket.
    *
    * @param order the order of this basket
    */
    @Override
    public void setOrder(int order) {
        _basket.setOrder(order);
    }

    @Override
    public boolean isNew() {
        return _basket.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _basket.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _basket.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _basket.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _basket.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _basket.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _basket.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _basket.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _basket.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _basket.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _basket.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new BasketWrapper((Basket) _basket.clone());
    }

    @Override
    public int compareTo(info.textgrid.model.Basket basket) {
        return _basket.compareTo(basket);
    }

    @Override
    public int hashCode() {
        return _basket.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<info.textgrid.model.Basket> toCacheModel() {
        return _basket.toCacheModel();
    }

    @Override
    public info.textgrid.model.Basket toEscapedModel() {
        return new BasketWrapper(_basket.toEscapedModel());
    }

    @Override
    public info.textgrid.model.Basket toUnescapedModel() {
        return new BasketWrapper(_basket.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _basket.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _basket.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _basket.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof BasketWrapper)) {
            return false;
        }

        BasketWrapper basketWrapper = (BasketWrapper) obj;

        if (Validator.equals(_basket, basketWrapper._basket)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Basket getWrappedBasket() {
        return _basket;
    }

    @Override
    public Basket getWrappedModel() {
        return _basket;
    }

    @Override
    public void resetOriginalValues() {
        _basket.resetOriginalValues();
    }
}
