package info.textgrid.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import info.textgrid.model.Basket;

import info.textgrid.service.BasketLocalServiceUtil;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class BasketActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public BasketActionableDynamicQuery() throws SystemException {
        setBaseLocalService(BasketLocalServiceUtil.getService());
        setClass(Basket.class);

        setClassLoader(info.textgrid.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("basketId");
    }
}
