package info.textgrid.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.textgrid.model.Basket;

/**
 * The persistence interface for the basket service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BasketPersistenceImpl
 * @see BasketUtil
 * @generated
 */
public interface BasketPersistence extends BasePersistence<Basket> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link BasketUtil} to access the basket persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the baskets where uuid = &#63;.
    *
    * @param uuid the uuid
    * @return the matching baskets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<info.textgrid.model.Basket> findByUuid(
        java.lang.String uuid)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the baskets where uuid = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link info.textgrid.model.impl.BasketModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param uuid the uuid
    * @param start the lower bound of the range of baskets
    * @param end the upper bound of the range of baskets (not inclusive)
    * @return the range of matching baskets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<info.textgrid.model.Basket> findByUuid(
        java.lang.String uuid, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the baskets where uuid = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link info.textgrid.model.impl.BasketModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param uuid the uuid
    * @param start the lower bound of the range of baskets
    * @param end the upper bound of the range of baskets (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching baskets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<info.textgrid.model.Basket> findByUuid(
        java.lang.String uuid, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first basket in the ordered set where uuid = &#63;.
    *
    * @param uuid the uuid
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching basket
    * @throws info.textgrid.NoSuchBasketException if a matching basket could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket findByUuid_First(java.lang.String uuid,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            info.textgrid.NoSuchBasketException;

    /**
    * Returns the first basket in the ordered set where uuid = &#63;.
    *
    * @param uuid the uuid
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching basket, or <code>null</code> if a matching basket could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket fetchByUuid_First(java.lang.String uuid,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last basket in the ordered set where uuid = &#63;.
    *
    * @param uuid the uuid
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching basket
    * @throws info.textgrid.NoSuchBasketException if a matching basket could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket findByUuid_Last(java.lang.String uuid,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            info.textgrid.NoSuchBasketException;

    /**
    * Returns the last basket in the ordered set where uuid = &#63;.
    *
    * @param uuid the uuid
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching basket, or <code>null</code> if a matching basket could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket fetchByUuid_Last(java.lang.String uuid,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the baskets before and after the current basket in the ordered set where uuid = &#63;.
    *
    * @param basketId the primary key of the current basket
    * @param uuid the uuid
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next basket
    * @throws info.textgrid.NoSuchBasketException if a basket with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket[] findByUuid_PrevAndNext(long basketId,
        java.lang.String uuid,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            info.textgrid.NoSuchBasketException;

    /**
    * Removes all the baskets where uuid = &#63; from the database.
    *
    * @param uuid the uuid
    * @throws SystemException if a system exception occurred
    */
    public void removeByUuid(java.lang.String uuid)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of baskets where uuid = &#63;.
    *
    * @param uuid the uuid
    * @return the number of matching baskets
    * @throws SystemException if a system exception occurred
    */
    public int countByUuid(java.lang.String uuid)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the baskets where userId = &#63;.
    *
    * @param userId the user ID
    * @return the matching baskets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<info.textgrid.model.Basket> findByUser(long userId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the baskets where userId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link info.textgrid.model.impl.BasketModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param userId the user ID
    * @param start the lower bound of the range of baskets
    * @param end the upper bound of the range of baskets (not inclusive)
    * @return the range of matching baskets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<info.textgrid.model.Basket> findByUser(long userId,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the baskets where userId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link info.textgrid.model.impl.BasketModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param userId the user ID
    * @param start the lower bound of the range of baskets
    * @param end the upper bound of the range of baskets (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching baskets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<info.textgrid.model.Basket> findByUser(long userId,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first basket in the ordered set where userId = &#63;.
    *
    * @param userId the user ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching basket
    * @throws info.textgrid.NoSuchBasketException if a matching basket could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket findByUser_First(long userId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            info.textgrid.NoSuchBasketException;

    /**
    * Returns the first basket in the ordered set where userId = &#63;.
    *
    * @param userId the user ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching basket, or <code>null</code> if a matching basket could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket fetchByUser_First(long userId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last basket in the ordered set where userId = &#63;.
    *
    * @param userId the user ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching basket
    * @throws info.textgrid.NoSuchBasketException if a matching basket could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket findByUser_Last(long userId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            info.textgrid.NoSuchBasketException;

    /**
    * Returns the last basket in the ordered set where userId = &#63;.
    *
    * @param userId the user ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching basket, or <code>null</code> if a matching basket could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket fetchByUser_Last(long userId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the baskets before and after the current basket in the ordered set where userId = &#63;.
    *
    * @param basketId the primary key of the current basket
    * @param userId the user ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next basket
    * @throws info.textgrid.NoSuchBasketException if a basket with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket[] findByUser_PrevAndNext(long basketId,
        long userId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            info.textgrid.NoSuchBasketException;

    /**
    * Removes all the baskets where userId = &#63; from the database.
    *
    * @param userId the user ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByUser(long userId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of baskets where userId = &#63;.
    *
    * @param userId the user ID
    * @return the number of matching baskets
    * @throws SystemException if a system exception occurred
    */
    public int countByUser(long userId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the basket where userId = &#63; and textgridUri = &#63; or throws a {@link info.textgrid.NoSuchBasketException} if it could not be found.
    *
    * @param userId the user ID
    * @param textgridUri the textgrid uri
    * @return the matching basket
    * @throws info.textgrid.NoSuchBasketException if a matching basket could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket findByBasket(long userId,
        java.lang.String textgridUri)
        throws com.liferay.portal.kernel.exception.SystemException,
            info.textgrid.NoSuchBasketException;

    /**
    * Returns the basket where userId = &#63; and textgridUri = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param userId the user ID
    * @param textgridUri the textgrid uri
    * @return the matching basket, or <code>null</code> if a matching basket could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket fetchByBasket(long userId,
        java.lang.String textgridUri)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the basket where userId = &#63; and textgridUri = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param userId the user ID
    * @param textgridUri the textgrid uri
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching basket, or <code>null</code> if a matching basket could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket fetchByBasket(long userId,
        java.lang.String textgridUri, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the basket where userId = &#63; and textgridUri = &#63; from the database.
    *
    * @param userId the user ID
    * @param textgridUri the textgrid uri
    * @return the basket that was removed
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket removeByBasket(long userId,
        java.lang.String textgridUri)
        throws com.liferay.portal.kernel.exception.SystemException,
            info.textgrid.NoSuchBasketException;

    /**
    * Returns the number of baskets where userId = &#63; and textgridUri = &#63;.
    *
    * @param userId the user ID
    * @param textgridUri the textgrid uri
    * @return the number of matching baskets
    * @throws SystemException if a system exception occurred
    */
    public int countByBasket(long userId, java.lang.String textgridUri)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the basket in the entity cache if it is enabled.
    *
    * @param basket the basket
    */
    public void cacheResult(info.textgrid.model.Basket basket);

    /**
    * Caches the baskets in the entity cache if it is enabled.
    *
    * @param baskets the baskets
    */
    public void cacheResult(java.util.List<info.textgrid.model.Basket> baskets);

    /**
    * Creates a new basket with the primary key. Does not add the basket to the database.
    *
    * @param basketId the primary key for the new basket
    * @return the new basket
    */
    public info.textgrid.model.Basket create(long basketId);

    /**
    * Removes the basket with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param basketId the primary key of the basket
    * @return the basket that was removed
    * @throws info.textgrid.NoSuchBasketException if a basket with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket remove(long basketId)
        throws com.liferay.portal.kernel.exception.SystemException,
            info.textgrid.NoSuchBasketException;

    public info.textgrid.model.Basket updateImpl(
        info.textgrid.model.Basket basket)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the basket with the primary key or throws a {@link info.textgrid.NoSuchBasketException} if it could not be found.
    *
    * @param basketId the primary key of the basket
    * @return the basket
    * @throws info.textgrid.NoSuchBasketException if a basket with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket findByPrimaryKey(long basketId)
        throws com.liferay.portal.kernel.exception.SystemException,
            info.textgrid.NoSuchBasketException;

    /**
    * Returns the basket with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param basketId the primary key of the basket
    * @return the basket, or <code>null</code> if a basket with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public info.textgrid.model.Basket fetchByPrimaryKey(long basketId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the baskets.
    *
    * @return the baskets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<info.textgrid.model.Basket> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the baskets.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link info.textgrid.model.impl.BasketModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of baskets
    * @param end the upper bound of the range of baskets (not inclusive)
    * @return the range of baskets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<info.textgrid.model.Basket> findAll(int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the baskets.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link info.textgrid.model.impl.BasketModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of baskets
    * @param end the upper bound of the range of baskets (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of baskets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<info.textgrid.model.Basket> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the baskets from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of baskets.
    *
    * @return the number of baskets
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
