package info.textgrid.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link BasketLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see BasketLocalService
 * @generated
 */
public class BasketLocalServiceWrapper implements BasketLocalService,
    ServiceWrapper<BasketLocalService> {
    private BasketLocalService _basketLocalService;

    public BasketLocalServiceWrapper(BasketLocalService basketLocalService) {
        _basketLocalService = basketLocalService;
    }

    /**
    * Adds the basket to the database. Also notifies the appropriate model listeners.
    *
    * @param basket the basket
    * @return the basket that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public info.textgrid.model.Basket addBasket(
        info.textgrid.model.Basket basket)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.addBasket(basket);
    }

    /**
    * Creates a new basket with the primary key. Does not add the basket to the database.
    *
    * @param basketId the primary key for the new basket
    * @return the new basket
    */
    @Override
    public info.textgrid.model.Basket createBasket(long basketId) {
        return _basketLocalService.createBasket(basketId);
    }

    /**
    * Deletes the basket with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param basketId the primary key of the basket
    * @return the basket that was removed
    * @throws PortalException if a basket with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public info.textgrid.model.Basket deleteBasket(long basketId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.deleteBasket(basketId);
    }

    /**
    * Deletes the basket from the database. Also notifies the appropriate model listeners.
    *
    * @param basket the basket
    * @return the basket that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public info.textgrid.model.Basket deleteBasket(
        info.textgrid.model.Basket basket)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.deleteBasket(basket);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _basketLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link info.textgrid.model.impl.BasketModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link info.textgrid.model.impl.BasketModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public info.textgrid.model.Basket fetchBasket(long basketId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.fetchBasket(basketId);
    }

    /**
    * Returns the basket with the primary key.
    *
    * @param basketId the primary key of the basket
    * @return the basket
    * @throws PortalException if a basket with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public info.textgrid.model.Basket getBasket(long basketId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.getBasket(basketId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the baskets.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link info.textgrid.model.impl.BasketModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of baskets
    * @param end the upper bound of the range of baskets (not inclusive)
    * @return the range of baskets
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<info.textgrid.model.Basket> getBaskets(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.getBaskets(start, end);
    }

    /**
    * Returns the number of baskets.
    *
    * @return the number of baskets
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getBasketsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.getBasketsCount();
    }

    /**
    * Updates the basket in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param basket the basket
    * @return the basket that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public info.textgrid.model.Basket updateBasket(
        info.textgrid.model.Basket basket)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.updateBasket(basket);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _basketLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _basketLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _basketLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public info.textgrid.model.Basket addBasket(long userId,
        java.lang.String textgridUri)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _basketLocalService.addBasket(userId, textgridUri);
    }

    @Override
    public info.textgrid.model.Basket deleteBasket(long userId,
        java.lang.String textgridUri)
        throws com.liferay.portal.kernel.exception.SystemException,
            info.textgrid.NoSuchBasketException {
        return _basketLocalService.deleteBasket(userId, textgridUri);
    }

    @Override
    public java.util.List<info.textgrid.model.Basket> findByUser(long userId) {
        return _basketLocalService.findByUser(userId);
    }

    @Override
    public java.util.List<java.lang.String> getUrlsByUser(long userId) {
        return _basketLocalService.getUrlsByUser(userId);
    }

    @Override
    public void deleteAllBasketsByUser(long userId) {
        _basketLocalService.deleteAllBasketsByUser(userId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public BasketLocalService getWrappedBasketLocalService() {
        return _basketLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedBasketLocalService(
        BasketLocalService basketLocalService) {
        _basketLocalService = basketLocalService;
    }

    @Override
    public BasketLocalService getWrappedService() {
        return _basketLocalService;
    }

    @Override
    public void setWrappedService(BasketLocalService basketLocalService) {
        _basketLocalService = basketLocalService;
    }
}
