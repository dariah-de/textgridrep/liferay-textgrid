package info.textgrid.repportlet.usersettings;

import java.io.Serializable;

public class UserSettings implements Serializable{ 

	private static final long serialVersionUID = 1786520003840881119L;
	
	private Boolean sandboxEnabled;
	private Boolean annotationEnabled;

	public Boolean getSandboxEnabled() {
		return sandboxEnabled;
	}

	public void setSandboxEnabled(Boolean sandboxEnabled) {
		this.sandboxEnabled = sandboxEnabled;
	}

	public Boolean getAnnotationEnabled() {
		return annotationEnabled;
	}

	public void setAnnotationEnabled(Boolean annotationEnabled) {
		this.annotationEnabled = annotationEnabled;
	}
	
}
