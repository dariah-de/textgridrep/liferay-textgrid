package info.textgrid.repportlet.basketcounter;

import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;

import java.util.List;

import info.textgrid.service.BasketLocalServiceUtil;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PortalUtil;

@Controller
@RequestMapping("VIEW")
public class BasketCounterController {
	
	private static final Log log = LogFactoryUtil.getLog(BasketCounterController.class);
	
	@RenderMapping
	public String render(Model model, PortletSession session, RenderRequest renderRequest) {
		
		long userId = PortalUtil.getUserId(renderRequest);
		int count;
		
		if(userId > 0) {
			count = BasketLocalServiceUtil.getUrlsByUser(userId).size();
		} else {
			if (session.getAttribute("basket", PortletSession.APPLICATION_SCOPE) == null) {
				count = 0;
			} else {
				count = ((List<String>) session.getAttribute("basket", PortletSession.APPLICATION_SCOPE)).size();
			}
		}
		
		model.addAttribute("basketcount", count);
		
		return "basketCountJs";
	}

}
