package info.textgrid.repportlet.shared;


/**
 * i18n:
 * http://stackoverflow.com/a/3717539
 * http://stackoverflow.xluat.com/questions/7469223/jsp-and-resourcebundles
 * http://java.dzone.com/articles/resource-bundle-tricks-and
 * 
 */
public class Pager {

	private int hits, limit, start, currentPage, totalPages;
	//private String query;
	
    private int pageRange = 8; // Default page range (max amount of page links to be displayed at once).
    private Integer[] pages;
	
	public void calculatePages() {

		//hits = Integer.parseInt(response.getHits());
		//limit = Integer.parseInt(response.getLimit());
		//results = response.getResult();
		
        // Set currentPage, totalPages and pages.
        currentPage = (hits / limit) - ((hits - start) / limit) + 1;
        totalPages = (hits / limit) + ((hits % limit != 0) ? 1 : 0);
        int pagesLength = Math.min(pageRange, totalPages);
        pages = new Integer[pagesLength];

        // firstPage must be greater than 0 and lesser than totalPages-pageLength.
        int firstPage = Math.min(Math.max(0, currentPage - (pageRange / 2)), totalPages - pagesLength);

        // Create pages (page numbers for page links).
        for (int i = 0; i < pagesLength; i++) {
            pages[i] = ++firstPage;
        }
		
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getHits() {
		return hits;
	}

	public void setHits(int hits) {
		this.hits = hits;
	}
	
	public Integer[] getPages() {
	    return pages;
	}
	
	public int getTotalPages() {
		return totalPages;
	}

/*	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
*/	
	
}
