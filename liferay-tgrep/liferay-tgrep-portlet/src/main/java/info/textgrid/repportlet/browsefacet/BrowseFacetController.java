package info.textgrid.repportlet.browsefacet;

import info.textgrid.namespaces.middleware.tgsearch.FacetType;
import info.textgrid.repportlet.basket.BasketAjaxController;
import info.textgrid.repportlet.shared.I18NUtils;
import info.textgrid.repportlet.shared.Utils;
import info.textgrid.service.tgsearch.TgsearchClientService;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.RenderRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PortalUtil;

@Controller
@RequestMapping("VIEW")
public class BrowseFacetController extends BasketAjaxController {

	// injected service
	private TgsearchClientService tgsearchClientService;
	
	private static final Log log = LogFactoryUtil.getLog(BrowseFacetController.class);

    @Autowired
    public BrowseFacetController(TgsearchClientService tgsearchClientService) {
        this.tgsearchClientService = tgsearchClientService;
    }

	@RenderMapping
	public String render(Model model, RenderRequest renderRequest) {

	    boolean sandbox = Utils.userWantsSandbox(PortalUtil.getUserId(renderRequest));

	    // list facets configured for this portlet
		List<String> facetList = new ArrayList<String>();
		facetList.add(renderRequest.getPreferences().getValue("facet", ""));
		int facetLimit = Integer.parseInt(renderRequest.getPreferences().getValue("limit", ""));
		String order = renderRequest.getPreferences().getValue("order", "");

		List<FacetType> facets = tgsearchClientService.listFacet(facetList, facetLimit, order, sandbox).getFacetGroup().get(0).getFacet();
		model.addAttribute("i18n", I18NUtils.i18n(renderRequest));
		model.addAttribute("facets", facets);
		model.addAttribute("facet", renderRequest.getPreferences().getValue("facet", ""));
		return "browsefacet";
	}
	
}
