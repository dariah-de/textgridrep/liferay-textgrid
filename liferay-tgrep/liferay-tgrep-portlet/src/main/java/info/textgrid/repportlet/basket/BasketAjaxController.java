package info.textgrid.repportlet.basket;

import info.textgrid.NoSuchBasketException;
import info.textgrid.service.BasketLocalServiceUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.util.PortalUtil;

@Controller(value = "AjaxController") 
@RequestMapping("VIEW") 
public class BasketAjaxController {
	
	@ResourceMapping(value="addToBasket")
	public void addToBasket(ResourceRequest request, PortletSession session, ResourceResponse response) throws IOException, SystemException {

		String textgridUri = request.getParameter("uri");
		int basketSize = 0;
		
		long userId = PortalUtil.getUserId(request);
		if(userId > 0) {
			BasketLocalServiceUtil.addBasket(userId, textgridUri);
			basketSize = BasketLocalServiceUtil.findByUser(userId).size();
		} else {
			List<String> basket;
			
			if(session.getAttribute("basket", PortletSession.APPLICATION_SCOPE) == null) {
				basket = new ArrayList<String>();
			} else {
				basket = (List<String>) session.getAttribute("basket", PortletSession.APPLICATION_SCOPE);
			}
			basket.add(textgridUri);
			session.setAttribute("basket", basket, PortletSession.APPLICATION_SCOPE);
			basketSize = basket.size();
			
		}
			
		JSONObject json = JSONFactoryUtil.createJSONObject();
		json.put("added", textgridUri);
		json.put("basketSize", basketSize);
		response.getWriter().println(json);
		
	}


	@ResourceMapping(value="removeFromBasket")
	public void removeFromBasket(ResourceRequest request, PortletSession session, ResourceResponse response) throws IOException, SystemException, NoSuchBasketException {
		
		String textgridUri = request.getParameter("uri");
		int basketSize = 0;
		
		long userId = PortalUtil.getUserId(request);
		if(userId > 0) {
			BasketLocalServiceUtil.deleteBasket(userId, textgridUri);
			basketSize = BasketLocalServiceUtil.findByUser(userId).size();
		} else {
			List<String> basket = (List<String>) session.getAttribute("basket", PortletSession.APPLICATION_SCOPE);
			basket.remove(textgridUri);
			session.setAttribute("basket", basket, PortletSession.APPLICATION_SCOPE);
			basketSize = basket.size();
		}
			
		JSONObject json = JSONFactoryUtil.createJSONObject();
		json.put("deleted", textgridUri);
		json.put("basketSize", basketSize);
		response.getWriter().println(json);
		
	}

	@ResourceMapping(value="clearBasket")
	public void clearBasket(ResourceRequest request, PortletSession session, ResourceResponse response) throws IOException, SystemException, NoSuchBasketException {
		
		String textgridUri = request.getParameter("uri");
		long userId = PortalUtil.getUserId(request);
		
		if(userId > 0) {
			BasketLocalServiceUtil.deleteAllBasketsByUser(userId);
		} else {
			session.setAttribute("basket", new ArrayList<String>(), PortletSession.APPLICATION_SCOPE);
		}
			
		JSONObject json = JSONFactoryUtil.createJSONObject();
		json.put("clear", "done");
		response.getWriter().println(json);
		
	}

}
