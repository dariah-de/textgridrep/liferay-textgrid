package info.textgrid.repportlet.search;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletPreferences;
import javax.portlet.ReadOnlyException;
import javax.portlet.ValidatorException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;


@Controller
@RequestMapping("EDIT")
public class SearchConfigController {
	
	private static final Log log = LogFactoryUtil.getLog(SearchConfigController.class);

	private static String[] fields = {
			"tgsearchServiceUrl"
	};
	
    /**
     * @param model
     * @param prefs
     * @return
     */
    @RenderMapping
    public String render(Model model, PortletPreferences prefs) {
    	
    	for(String field: fields) {
    		model.addAttribute(field, prefs.getValue(field, ""));
    	}   	
    	
        return "searchConfig";
    }
    
    /**
     * @param request
     * @param prefs
     * @param resp
     * @throws ReadOnlyException
     * @throws ValidatorException
     * @throws IOException
     */
    @ActionMapping(value = "submitFormURL")
    public void storePrefs( 
    		ActionRequest request,
            PortletPreferences prefs,
            ActionResponse resp) throws ReadOnlyException, ValidatorException, IOException {
    	
    	for(String field: fields) {
    		prefs.setValue(field, request.getParameter(field));
		}

		prefs.store();
	}

	/**
	 * @param prefs
	 * @param resp
	 * @throws ReadOnlyException
	 * @throws ValidatorException
	 * @throws IOException
	 */
	@ActionMapping(value = "resetFormURL")
	public void resetPrefs(PortletPreferences prefs, ActionResponse resp)
			throws ReadOnlyException, ValidatorException, IOException {

		for (String field : fields) {
			prefs.reset(field);
		}

		prefs.store();
	}

}
