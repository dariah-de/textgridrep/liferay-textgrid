package info.textgrid.repportlet.browse;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.ClientBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PortalUtil;

import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import info.textgrid.repportlet.basket.BasketAjaxController;
import info.textgrid.repportlet.shared.I18NUtils;
import info.textgrid.repportlet.shared.Utils;
import info.textgrid.repportlet.shared.ViewMode;
import info.textgrid.service.tgsearch.AggregatorClientService;
import info.textgrid.service.tgsearch.TgrepConfigurationService;
import info.textgrid.service.tgsearch.TgsearchClientService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Controller
@RequestMapping("VIEW")
public class BrowseController extends BasketAjaxController {

	// injected service
	private TgsearchClientService tgsearchClientService;
	private TgrepConfigurationService tgrepConfig;
	private AggregatorClientService aggregatorClient;

	private static final Log log = LogFactoryUtil.getLog(BrowseController.class);

    @Autowired
    public BrowseController(TgsearchClientService tgsearchClientService,
    		AggregatorClientService aggregatorClient, TgrepConfigurationService tgrepConfig) {
        this.tgsearchClientService = tgsearchClientService;
        this.aggregatorClient = aggregatorClient;
        this.tgrepConfig = tgrepConfig;
    }

	@RenderMapping
	public String render(Model model, RenderRequest renderRequest,
			@RequestParam(value="id", required=false) String id,
			@RequestParam(value="mode",  defaultValue="list") String mode,
			PortletPreferences prefs
			) {

		HashMap<String, String> i18n = I18NUtils.i18n(renderRequest);

	    HttpServletRequest r = PortalUtil.getHttpServletRequest(renderRequest);
	    HttpServletRequest sr = PortalUtil.getOriginalServletRequest(r);

	    // common variables for browse-root aggregations and browse single items
	    model.addAttribute("textgridHost", this.tgrepConfig.getTextgridHost());
	    model.addAttribute("mode", mode);
	    model.addAttribute("i18n", i18n);


	    // if no id, browse all root aggregations
		if(id==null || id.equals("root")) {
			List<ViewMode> viewmodes = new ArrayList<ViewMode>();
			viewmodes.add(new ViewMode(i18n.get("list"), "/browse/-/browse/root/list", mode.equals("list")));
			viewmodes.add(new ViewMode(i18n.get("gallery"), "/browse/-/browse/root/gallery", mode.equals("gallery")));
			model.addAttribute("viewmodes", viewmodes);

			List<ResultType> results = this.tgsearchClientService.listRootCollections().getResult();
			model.addAttribute("results", results);
			model.addAttribute("browseRootAggregations", true);
			return "browse";
		}

		if(!id.startsWith("textgrid:")) {
			id = "textgrid:"+id;
		}
		id = id.replace("_", ".");

		model.addAttribute("textgridUri", id);

		ResultType metadata = this.tgsearchClientService.getMetadata(id);
		model.addAttribute("metadata", metadata);

		// for path, todo: create a tagfile for path
		model.addAttribute("result", metadata);

		String format = metadata.getObject().getGeneric().getProvided().getFormat();
		model.addAttribute("format", format);

		if(format.contains("tg.aggregation") || format.contains("text/tg.work+xml")) {

			List<ViewMode> viewmodes = new ArrayList<ViewMode>();

			viewmodes.add(new ViewMode(i18n.get("list"), Utils.browseUrl(id)+ "/list", mode.equals("list")));
			viewmodes.add(new ViewMode(i18n.get("gallery"), Utils.browseUrl(id)+ "/gallery", mode.equals("gallery")));

			if(format.equals("text/tg.edition+tg.aggregation+xml")) {
				viewmodes.add(new ViewMode("TEI-Corpus", Utils.browseUrl(id)+ "/xml", mode.equals("xml")));
			}

			model.addAttribute("viewmodes", viewmodes);

			if(mode != null && mode.equals("xml")) {
				String teiHtml = this.aggregatorClient.renderTEI(id);
				String tocHtml = this.aggregatorClient.renderToc(id);
				model.addAttribute("teiHtml", teiHtml);
				model.addAttribute("tocHtml", tocHtml);

			} else {
				log.info("listing agg: " + id);

				List<ResultType> results;
				if(format.contains("text/tg.work+xml")) {
					List<String> editionFilter = Arrays.asList(new String[] {"format:text/tg.edition+tg.aggregation+xml"});
					results = this.tgsearchClientService.search("isEditionOf:\""+id+"\"", "relevance", 0, 20, editionFilter, true).getResult();
				} else {
					results = this.tgsearchClientService.listAggregation(id).getResult();
				}
				model.addAttribute("results", results);
			}
		} else if(format.equals("text/xml")) {

			List<ViewMode> tools = new ArrayList<ViewMode>();

		    // identifier@type of trier dfg-viewer mets is METSXMLID
			// TODO: look into relation/tg:rootElementNamespace
			if (    ! metadata.getObject().getGeneric().getProvided().getIdentifier().isEmpty()
					&& metadata.getObject().getGeneric().getProvided().getIdentifier().get(0) != null
					&& metadata.getObject().getGeneric().getProvided().getIdentifier().get(0).getType() != null
					&& metadata.getObject().getGeneric().getProvided().getIdentifier().get(0).getType().equals("METSXMLID")) {
				try {
					String tgcrudUrl4DFGViewer =   URLEncoder.encode(tgrepConfig.getTextgridHost() + "/1.0/tgcrud-public/rest/"+id+"/data", "UTF-8");
					tools.add(new ViewMode("DFG-Viewer", "http://dfg-viewer.de/v3/?set[zoom]=min&set[mets]="+tgcrudUrl4DFGViewer, false));
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else { // assume tei-xml

				String fragment = Utils.getParamWithDefault(sr, "fragment", "");
				String teiHtml = "";

				if(fragment != "") {
					teiHtml = this.aggregatorClient.renderTEIFragment(id, fragment);
				} else {
					teiHtml = this.aggregatorClient.renderTEI(id);
					String tocHtml = this.aggregatorClient.renderToc(id);
					model.addAttribute("tocHtml", tocHtml);
				}
				model.addAttribute("teiHtml", teiHtml);
				model.addAttribute("isTEI", true);

			}

            // TOOL: VOYANT
            tools.add(new ViewMode("Voyant (XML)", "https://voyant-tools.org?input=" + tgrepConfig.getTextgridHost() + "/1.0/tgcrud-public/rest/" + id + "/data", false));

            // TOOL: SWITCHBOARD
            try {
                String text4switchboard = URLEncoder.encode(tgrepConfig.getTextgridHost() + "/1.0/aggregator/text/" + id, "UTF-8");
                tools.add(new ViewMode("Switchboard (txt)", "https://switchboard.clarin-dev.eu/#/textgridrep/" + text4switchboard + "/text%2Fplain", false));
            } catch (UnsupportedEncodingException e) {
                log.error("error encoding url for switchboard", e);
            }
            
            // TOOL: IIIF MANIFEST
			if(hasIiifManifest(metadata.getObject().getGeneric().getGenerated().getProject().getId())) {
				tools.add(new ViewMode("Mirador", tgrepConfig.getTextgridHost() + "/1.0/iiif/mirador/?uri=" + id, false));
				tools.add(new ViewMode("<img style='margin-top:2px; height: 21px;' title='Drop icon on Mirador to open manifest' src='https://textgridlab.org/1.0/iiif/manifests/img/iiif-logo.svg'>", tgrepConfig.getTextgridHost() + "/1.0/iiif/manifests/" + id + "/manifest.json", false));
			}

            // TOOL: ANNOTATE
			tools.add(new ViewMode("Annotate", "https://annotation.de.dariah.eu/AnnotationViewer/data.html?uri=" + id, false, "annotation-button"));


			model.addAttribute("tools", tools);

		} else if(format.contains("image")) {
			model.addAttribute("image", true);
			List<ViewMode> tools = new ArrayList<ViewMode>();
			tools.add(new ViewMode("Digilib", tgrepConfig.getTextgridHost() + "/1.0/tgrep/digilib/digilib.html?fn="+id, false));
			model.addAttribute("tools", tools);
		}

		long userId = PortalUtil.getUserId(renderRequest);
		String userName = PortalUtil.getUserName(userId, "");
		String eppn = Utils.getEppn(userId);

		if(Utils.userWantsAnnotation(userId)) {
			model.addAttribute("annotationEnabled", true);
			model.addAttribute("annotationToken", this.generateToken(prefs, eppn, userName));
		}

		return "browse";
	}

	private boolean hasIiifManifest(String id) {

		IIIFProjects iiip = ClientBuilder.newBuilder().build()
			.register(JacksonJsonProvider.class)
			.target(tgrepConfig.getTextgridHost())
			.path("/1.0/iiif/manifests/projects/")
			.request()
			.get()
			.readEntity(IIIFProjects.class);

		if(iiip.projects.contains(id)) {
			return true;
		}

		return false;
	}

    /* TODO: JWT generation */
	public static String tokenFromAuthstring(String authString, String secret) {
		Jws<Claims> jwtres = Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(authString);
		String tstring = jwtres.getBody().get("token").toString();
		String token = tstring.substring(tstring.lastIndexOf("@")+1);
		return token;
	}

	public static String generateToken(PortletPreferences prefs, String userId, String displayName) {

		String consumerKey = prefs.getValue("dariahAnnotationKey", "");
		String consumerSecret = prefs.getValue("dariahAnnotationSecret", "");

		if(consumerKey.equals("") || consumerSecret.equals("")) {
			System.out.println("dariah annotation key and secret need to be set");
			return "";
		}

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));

		return Jwts.builder()
			.claim("consumerKey", consumerKey)
			.claim("userId", userId)
			.claim("displayName", displayName)
			.claim("memberOf", new String[0])
			.claim("issuedAt", df.format(new Date()))
			.claim("ttl", "86400")
			.signWith(SignatureAlgorithm.HS256, consumerSecret.getBytes())
			.compact();
	}
}
