package info.textgrid.repportlet.shared;

import info.textgrid.namespaces.metadata.core._2010.RelationType;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import info.textgrid.service.BasketLocalServiceUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.servlet.http.HttpServletRequest;

import org.w3._1999._02._22_rdf_syntax_ns_.RdfType;
import org.w3c.dom.Element;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.security.auth.CompanyThreadLocal;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;



public class Utils {
	
	private final static int THUMBSIZE = 250;
	//private final static String BROWSE_URL = "/web/textgrid/browse/-/tgrep/"; 
	private final static String BROWSE_URL = "/browse/-/browse/";
	private final static String SEARCH_URL = "/search/-/search/";
	
	private static final Log log = LogFactoryUtil.getLog(Utils.class);
	
	public static String browseUrl(String tgurl) {
		return BROWSE_URL + urlmod(tgurl);
	}
	
	public static String searchUrl(String mode, String query, List<String> filter, String order, int start, int limit) {
		String querystring = query.replace("\"", "&quot;")+getFilterQueryString(filter);
		return SEARCH_URL + mode + "?query="+querystring+"&start="+start+"&limit="+limit+"&order="+order ;
	}

	public static String urlmod(String tgurl) {
		//System.out.println("urmlod called with uri: " + tgurl);
		if(tgurl.length() > 10) {
			return tgurl.substring(9).replace('.', '_');
		}
		return "tgurl";
	}
	
	public static String getImageUrl(String textgridHost, ResultType tgsearchresult) {
	
		String imageUri = null;
		
		if(tgsearchresult.getObject().getGeneric().getProvided().getFormat().contains("image")) {
			imageUri = tgsearchresult.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
			
		}else if(tgsearchresult.getObject().getGeneric().getProvided().getFormat().contains("tg.collection")) {
            // collections may have author images linked in the "digitale bibliothek"
			RelationType rel = tgsearchresult.getObject().getRelations();
	        if (rel != null) {
	            RdfType rdf = rel.getRDF();
	            if (rdf != null) {
	                List<Object> rdfs = rdf.getAny();
	                for (Object o : rdfs) {
	                    Element dom = (Element) o;
	                    imageUri = dom
	                    		.getElementsByTagNameNS("http://textgrid.info/relation-ns#", "depiction")
	                    		.item(0)
	                    		.getAttributes()
	                    		.getNamedItemNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource")
	                    		.getTextContent();

	                }
	            }
	        }
		} 
		
        if (imageUri != null) {
        	return textgridHost + "/1.0/digilib/rest/IIIF/"+imageUri+"/full/,"+THUMBSIZE+"/0/native.jpg";
        } else {
			return "https://upload.wikimedia.org/wikipedia/commons/1/1d/No_image.svg";
		}
	}
	
	public static String getParamWithDefault(HttpServletRequest sr, String param, String defaultString) {
		if(sr.getParameter(param) == null) {
			return defaultString;
		}
		return sr.getParameter(param);
	}
	
	public static String getFilterQueryString(List<String> filters) {

		if(filters==null) {
			return "";
		}
		
		StringBuffer sb = new StringBuffer();
		for(String filter : filters) {
			try {
				filter = URLEncoder.encode(filter, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				log.error("could not urlencode the string: " + filter);
			}
			sb.append("&filter=").append(filter).append("");
		}
		return sb.toString();
	}
	
	
	public static List<String> getBasketItems(PortletRequest request) {
		long userId = PortalUtil.getUserId(request);
		if(userId > 0) {
			return BasketLocalServiceUtil.getUrlsByUser(userId);
		} else {
			PortletSession session = request.getPortletSession();
			return (List<String>) session.getAttribute("basket", PortletSession.APPLICATION_SCOPE);
		}		
	}
	
	public static String getBasketItemsAsString(PortletRequest request) {
		return join(getBasketItems(request), ",");
	}
	
	// http://stackoverflow.com/a/63274
	public static String join(List<String> list, String delim) {
	
	    StringBuilder sb = new StringBuilder();
	    String loopDelim = "";
	    
	    if(list != null) {
		    for(String s : list) {
		         sb.append(loopDelim);
		         sb.append(s);
		         loopDelim = delim;
		    }
		}
	
	    return sb.toString();
	}
	
	public static String urlencode(String string) {
		try {
			string = URLEncoder.encode(string, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.error("could not urlencode the string: " + string);
		}		
		return string;
	}
	
	public static Boolean userWantsSandbox(long userId) {
		boolean sandbox = false;
		ExpandoValue expandoObject = null;
		try {
			expandoObject = ExpandoValueLocalServiceUtil.getValue(CompanyThreadLocal.getCompanyId(), 
					"com.liferay.portal.model.User", "CUSTOM_FIELDS", "textgrid-sandbox-results", userId);
			if(expandoObject != null){
				sandbox = expandoObject.getBoolean();
			}
		} catch (SystemException | PortalException e) {
			log.error("error reading expandoValue (textgrid-sandbox-results)", e);
		} 
		return sandbox;
	}
	
	public static Boolean userWantsAnnotation(long userId) {
		boolean annotation = false;
		ExpandoValue expandoObject = null;
		try {
			expandoObject = ExpandoValueLocalServiceUtil.getValue(CompanyThreadLocal.getCompanyId(), 
					"com.liferay.portal.model.User", "CUSTOM_FIELDS", "textgrid-annotation", userId);
			if(expandoObject != null){
				annotation = expandoObject.getBoolean();
			}
		} catch (SystemException | PortalException e) {
			log.error("error reading expandoValue (textgrid-annotation)", e);
		} 
		return annotation;
	}
	
	public static String getEppn(long userId) {
		String eppn = "";
		ExpandoValue expandoObject = null;
		try {
			expandoObject = ExpandoValueLocalServiceUtil.getValue(CompanyThreadLocal.getCompanyId(), 
					"com.liferay.portal.model.User", "CUSTOM_FIELDS", "eppn", userId);
			if(expandoObject != null){
				eppn = expandoObject.getString();
			}
		} catch (SystemException | PortalException e) {
			log.error("error reading expandoValue (eppn)", e);
		} 
		return eppn;
	}
	
    public static String replaceAll(String string, String pattern, String replacement) {
        return string.replaceAll(pattern, replacement);
    }

}
