package info.textgrid.repportlet.advancedsearch;

import javax.portlet.RenderRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

@Controller
@RequestMapping("VIEW")
public class AdvancedSearchController {
	
	private static final Log log = LogFactoryUtil.getLog(AdvancedSearchController.class);

	@RenderMapping
	public String render(Model model, RenderRequest renderRequest) {
		return "advancedsearch";
	}
	
}
