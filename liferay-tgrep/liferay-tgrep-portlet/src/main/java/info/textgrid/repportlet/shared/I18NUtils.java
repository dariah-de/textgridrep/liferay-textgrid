package info.textgrid.repportlet.shared;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import javax.portlet.RenderRequest;

public class I18NUtils {

	public static HashMap<String, String> i18n (RenderRequest renderRequest) {
		
		//System.out.println("lang: " + LanguageUtil.getLanguageId(renderRequest));
		//System.out.println("lang2: " + renderRequest.getLocale());
		
		HashMap<String, String> messages = new HashMap<String, String>();
		ResourceBundle rb = ResourceBundle.getBundle("i18n.Language", renderRequest.getLocale(), new UTF8Control());
		
		for(String key : rb.keySet()) {
			messages.put(key, rb.getString(key));
			//System.out.println(rb.getString(key));			
		}
		
		return messages;
	}
	
	// read resource bundle in utf8: http://stackoverflow.com/a/4660195
	private static class UTF8Control extends Control {
	    public ResourceBundle newBundle
	        (String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
	            throws IllegalAccessException, InstantiationException, IOException
	    {
	        // The below is a copy of the default implementation.
	        String bundleName = toBundleName(baseName, locale);
	        String resourceName = toResourceName(bundleName, "properties");
	        ResourceBundle bundle = null;
	        InputStream stream = null;
	        if (reload) {
	            URL url = loader.getResource(resourceName);
	            if (url != null) {
	                URLConnection connection = url.openConnection();
	                if (connection != null) {
	                    connection.setUseCaches(false);
	                    stream = connection.getInputStream();
	                }
	            }
	        } else {
	            stream = loader.getResourceAsStream(resourceName);
	        }
	        if (stream != null) {
	            try {
	                // Only this line is changed to make it to read properties files as UTF-8.
	                bundle = new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"));
	            } finally {
	                stream.close();
	            }
	        }
	        return bundle;
	    }
	}
	
}
