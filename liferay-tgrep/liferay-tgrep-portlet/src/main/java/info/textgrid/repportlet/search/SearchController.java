package info.textgrid.repportlet.search;

import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.repportlet.basket.BasketAjaxController;
import info.textgrid.repportlet.shared.I18NUtils;
import info.textgrid.repportlet.shared.Pager;
import info.textgrid.repportlet.shared.Utils;
import info.textgrid.repportlet.shared.ViewMode;
import info.textgrid.service.tgsearch.TgrepConfigurationService;
import info.textgrid.service.tgsearch.TgsearchClientService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.util.HtmlUtils;

import com.liferay.portal.util.PortalUtil;

/**
 * tag vs include, nice tag explanation:
 * http://stackoverflow.com/questions/1296235/jsp-tricks-to-make-templating-easier/3257426#3257426
 *
 */

@Controller
@RequestMapping("VIEW")
public class SearchController extends BasketAjaxController {
	
	private TgsearchClientService tgsearchClientService;
	private TgrepConfigurationService tgrepConfig;

    @Autowired
    public SearchController(TgsearchClientService tgsearchClientService, TgrepConfigurationService tgrepConfig) {
        this.tgsearchClientService = tgsearchClientService;
        this.tgrepConfig = tgrepConfig;
    }

	
	@RenderMapping
	public String render(Model model, RenderRequest renderRequest,
			@RequestParam(value="mode", defaultValue="list") String mode) {
		
		HashMap<String, String> i18n = I18NUtils.i18n(renderRequest);
		
		// GET Parameters (todo: possible to get this via annotation?)
	    HttpServletRequest r = PortalUtil.getHttpServletRequest(renderRequest);
	    HttpServletRequest sr = PortalUtil.getOriginalServletRequest(r);
	    
	    String query = Utils.getParamWithDefault(sr, "query", "");
	    String order = Utils.getParamWithDefault(sr, "order", "relevance");
	    int start = Integer.parseInt(Utils.getParamWithDefault(sr, "start", "0"));
	    int limit = Integer.parseInt(Utils.getParamWithDefault(sr, "limit", "10"));
	    List<String> filter = null;
	    if(sr.getParameterValues("filter") != null) {
	    	filter = Arrays.asList(sr.getParameterValues("filter"));
		}

		boolean sandbox = Utils.userWantsSandbox(PortalUtil.getUserId(renderRequest));
		String aggregatorSandboxParam = sandbox ? "&sandbox=true" : "";

		String realQueryString = query;
		if(query.equals("") && filter != null) {
			// the filter only request, e.g. from facet-browse
			realQueryString = "*";
		}
		Response res = this.tgsearchClientService.search(realQueryString, order, start, limit, filter, sandbox);
		
		Pager pager = new Pager();
		pager.setHits(Integer.parseInt(res.getHits()));
		pager.setLimit(limit);
		pager.setStart(start);
		pager.calculatePages();
		model.addAttribute("pager", pager);
		
		List<ViewMode> viewmodes = new ArrayList<ViewMode>();
		viewmodes.add(new ViewMode(i18n.get("list"), Utils.searchUrl("list", query, filter, order, start, limit), mode.equals("list")));
		viewmodes.add(new ViewMode(i18n.get("gallery"), Utils.searchUrl("gallery", query, filter, order, start, limit), mode.equals("gallery")));
		model.addAttribute("viewmodes", viewmodes);
		model.addAttribute("mode", mode);
		
		model.addAttribute("results", res.getResult());
		model.addAttribute("facets", res.getFacetResponse().getFacetGroup());
		model.addAttribute("query", HtmlUtils.htmlEscape(query));
		model.addAttribute("order", order);
		model.addAttribute("start", start);
		model.addAttribute("limit", limit);
		model.addAttribute("filter", filter);
		model.addAttribute("basket", Utils.getBasketItems(renderRequest));
		model.addAttribute("filterQueryString", Utils.getFilterQueryString(filter));
		model.addAttribute("aggregatorUrl", this.tgrepConfig.getTextgridHost() + "/1.0/aggregator");
		model.addAttribute("aggregatorSandboxParam", aggregatorSandboxParam);
		model.addAttribute("realQueryString", realQueryString);
		model.addAttribute("textgridHost", this.tgrepConfig.getTextgridHost());

		// translation array
		model.addAttribute("i18n", i18n);

		// which .jsp to render
		return "search";
	}

}