package info.textgrid.repportlet.usersettings;

import info.textgrid.repportlet.shared.I18NUtils;
import info.textgrid.repportlet.shared.Utils;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.security.auth.CompanyThreadLocal;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;

@Controller
@RequestMapping("VIEW")
public class UserSettingsController {
	
	private static final Log log = LogFactoryUtil.getLog(UserSettingsController.class);

	@RenderMapping
	public String render(Model model, PortletSession session, RenderRequest renderRequest) {
		
		long userId = PortalUtil.getUserId(renderRequest);
		Boolean sandbox = Utils.userWantsSandbox(userId);
		Boolean annotation = Utils.userWantsAnnotation(userId);
		
		UserSettings userSettings = new UserSettings();
		userSettings.setSandboxEnabled(sandbox);
		userSettings.setAnnotationEnabled(annotation);
		
		model.addAttribute("userSettings", userSettings);
		model.addAttribute("i18n", I18NUtils.i18n(renderRequest));
		
		return "userconfig";
		
	}
	
    @ActionMapping(value = "handleUserSettings")
    public void setUserSettings(
            @ModelAttribute("userSettings") UserSettings userSettings,
            ActionRequest actionRequest, 
            ActionResponse actionResponse,
            Model model) {

		long userId = PortalUtil.getUserId(actionRequest);
		long companyId = CompanyThreadLocal.getCompanyId();
		
		try {
			ExpandoValueLocalServiceUtil.addValue(companyId, "com.liferay.portal.model.User", 
					"CUSTOM_FIELDS", "textgrid-sandbox-results", userId, userSettings.getSandboxEnabled());
		} catch (PortalException | SystemException e) {
			log.error("error setting expandoValue (textgrid-sandbox-results)", e);
		}
/*
		try {
			ExpandoValueLocalServiceUtil.addValue(companyId, "com.liferay.portal.model.User", 
					"CUSTOM_FIELDS", "textgrid-annotation", userId, userSettings.getAnnotationEnabled());
		} catch (PortalException | SystemException e) {
			log.error("error setting expandoValue (textgrid-annotation)", e);
		}
*/		
        actionResponse.setRenderParameter("action", "success");

    }

}
