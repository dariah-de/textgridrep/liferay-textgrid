package info.textgrid.repportlet.basket;

import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import info.textgrid.repportlet.shared.I18NUtils;
import info.textgrid.repportlet.shared.Utils;
import info.textgrid.repportlet.shared.ViewMode;
import info.textgrid.service.BasketLocalServiceUtil;
import info.textgrid.service.tgsearch.TgrepConfigurationService;
import info.textgrid.service.tgsearch.TgsearchClientService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PortalUtil;

@Controller
@RequestMapping("VIEW")
public class BasketController extends BasketAjaxController {

	// injected service
	private TgsearchClientService tgsearchClientService;

	private TgrepConfigurationService tgrepConfig;

	private static final Log log = LogFactoryUtil.getLog(BasketController.class);

    @Autowired
    public BasketController(TgsearchClientService tgsearchClientService, TgrepConfigurationService tgrepConfig) {
        this.tgsearchClientService = tgsearchClientService;
        this.tgrepConfig = tgrepConfig;
    }

	@RenderMapping
	public String render(Model model, PortletSession session, RenderRequest renderRequest,
			@RequestParam(value="mode", required=false) String mode) {

		HashMap<String, String> i18n = I18NUtils.i18n(renderRequest);

		model.addAttribute("mode", mode);

		List<ResultType> results = new ArrayList<ResultType>();

		long userId = PortalUtil.getUserId(renderRequest);
		List<String> basket;
		if(userId > 0) {
			basket =  BasketLocalServiceUtil.getUrlsByUser(userId);
		} else {
			basket = (List<String>) session.getAttribute("basket", PortletSession.APPLICATION_SCOPE);
		}

		if(basket != null) {
			for(String textgridUri : basket) {
				ResultType res = tgsearchClientService.getMetadata(textgridUri);
				results.add(res);
			}
		}


		List<ViewMode> tools = new ArrayList<ViewMode>();
//		tools.add(new ViewMode("Voyant v1", "#", false, "digivoy-basket-button"));
		tools.add(new ViewMode("Voyant", "#", false, "digivoy2-basket-button"));
		model.addAttribute("tools", tools);


		List<ViewMode> viewmodes = new ArrayList<ViewMode>();
		viewmodes.add(new ViewMode(i18n.get("list"), "/shelf/-/shelf/list", false));
		viewmodes.add(new ViewMode(i18n.get("gallery"), "/shelf/-/shelf/gallery", false));
		model.addAttribute("viewmodes", viewmodes);

		model.addAttribute("results", results);
		model.addAttribute("basket", Utils.getBasketItems(renderRequest));
		model.addAttribute("aggregatorUrl", tgrepConfig.getTextgridHost()+"/1.0/aggregator");
		model.addAttribute("basketItemString", Utils.getBasketItemsAsString(renderRequest));
		model.addAttribute("textgridHost", this.tgrepConfig.getTextgridHost());

		// translation array
		model.addAttribute("i18n", i18n);

		return "basket";

	}

}
