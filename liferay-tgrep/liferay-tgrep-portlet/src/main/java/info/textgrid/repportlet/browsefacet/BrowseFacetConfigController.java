package info.textgrid.repportlet.browsefacet;

import java.io.IOException;

import javax.portlet.ActionResponse;
import javax.portlet.PortletPreferences;
import javax.portlet.ReadOnlyException;
import javax.portlet.ValidatorException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

@Controller
@RequestMapping("EDIT")
public class BrowseFacetConfigController {
	
	private static final Log log = LogFactoryUtil.getLog(BrowseFacetConfigController.class);
	
    @RenderMapping
     public String render(Model model, PortletPreferences prefs) {
    	
        model.addAttribute("facet", prefs.getValue("facet", ""));
        model.addAttribute("limit", prefs.getValue("limit", ""));
        model.addAttribute("order", prefs.getValue("order", ""));
        model.addAttribute("sandbox", prefs.getValue("sandbox", ""));
        return "browsefacetConfig";
        
    }

    @ActionMapping(value = "submitFormURL")
    public void storePrefs( 
    		@RequestParam("facet") String facet,
    		@RequestParam("limit") String limit,
    		@RequestParam("order") String order,
    		@RequestParam("sandbox") String sandbox,
            PortletPreferences prefs,
            ActionResponse resp) throws ReadOnlyException, ValidatorException, IOException {

    	prefs.setValue("facet", facet);
    	prefs.setValue("limit", limit);
    	prefs.setValue("order", order);
    	prefs.setValue("sandbox", sandbox);
    	
    	prefs.store();
    	
    }
    
    @ActionMapping(value = "resetFormURL")
    public void resetPrefs( 
            PortletPreferences prefs,
            ActionResponse resp) throws ReadOnlyException, ValidatorException, IOException {
    	
    	prefs.reset("facet");
    	prefs.reset("limit");
    	prefs.reset("order");
    	
    	prefs.store();
    	
    }    
	


}
