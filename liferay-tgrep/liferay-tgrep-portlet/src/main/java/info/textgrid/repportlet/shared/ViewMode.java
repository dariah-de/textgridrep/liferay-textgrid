package info.textgrid.repportlet.shared;

import java.io.Serializable;

public class ViewMode implements Serializable {

	private static final long serialVersionUID = -5848034800808088323L;
	
	private String url;
	private String label;
	private boolean active;
	private String cssClass;
	
	public ViewMode(String label, String url,  boolean active) {
		this.label = label;
		this.url = url;
		this.active = active;
	}
	
	public ViewMode(String label, String url, boolean active, String cssClass) {
		this.label = label;
		this.url = url;
		this.active = active;
		this.setCssClass(cssClass);
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}
	
}
