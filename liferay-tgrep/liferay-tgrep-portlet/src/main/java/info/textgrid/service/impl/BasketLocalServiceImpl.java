package info.textgrid.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import info.textgrid.NoSuchBasketException;
import info.textgrid.model.Basket;
import info.textgrid.service.base.BasketLocalServiceBaseImpl;

/**
 * The implementation of the basket local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.textgrid.service.BasketLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see info.textgrid.service.base.BasketLocalServiceBaseImpl
 * @see info.textgrid.service.BasketLocalServiceUtil
 */
public class BasketLocalServiceImpl extends BasketLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link info.textgrid.service.BasketLocalServiceUtil} to access the basket local service.
     */
	
	 public Basket addBasket(long userId, String textgridUri) throws SystemException{
		 long basketId = counterLocalService.increment(Basket.class.getName());
		 Basket basket = basketPersistence.create(basketId);
		 basket.setTextgridUri(textgridUri);
		 basket.setUserId(userId);
		 super.addBasket(basket);
		 return basket;
	 }

	 
	 public Basket deleteBasket(long userId, String textgridUri) throws NoSuchBasketException, SystemException {
		 return basketPersistence.removeByBasket(userId, textgridUri);
	 }
	 
	 public List<Basket> findByUser(long userId){
		 List<Basket> baskets = new ArrayList<Basket>();
		 try {
			baskets = basketPersistence.findByUser(userId);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return baskets;
	 }
	
	 public List<String> getUrlsByUser(long userId){
		 List<Basket> baskets = findByUser(userId);
		 List<String> urls = new ArrayList<String>(baskets.size());
		 for(int i = 0; i < baskets.size(); i++){
			 urls.add(baskets.get(i).getTextgridUri());
		 }
		 return urls;
	 }
	 
	 public void deleteAllBasketsByUser(long userId){
		 List<Basket> baskets = findByUser(userId);
		 for(int i = 0; i < baskets.size(); i++){
			 try {
				deleteBasket(baskets.get(i));
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
	 }
}
