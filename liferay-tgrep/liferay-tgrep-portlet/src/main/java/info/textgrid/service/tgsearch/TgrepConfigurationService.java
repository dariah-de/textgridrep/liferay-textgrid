package info.textgrid.service.tgsearch;

import java.io.File;

import javax.annotation.PostConstruct;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.event.ConfigurationEvent;
import org.apache.commons.configuration.event.ConfigurationListener;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.springframework.stereotype.Service;

@Service
public class TgrepConfigurationService {

	private String textgridHost = "https://textgridlab.org";
	//private boolean tgsearchConfigChanged;
	
	private static final String alternateConfLocation = "/etc/textgrid/tgrep/tgrep.properties";

	public String getTextgridHost() {
		return textgridHost;	
	}

	public void setTextgridHost(String textgridHost) {
		this.textgridHost = textgridHost;
	}

	@PostConstruct
	public void postConstruct() throws ConfigurationException {
		
		setupConf();
		
	}
	
	private void setupConf() throws ConfigurationException {
		
		File altConfFile = new File(alternateConfLocation);
		if(altConfFile.exists() && altConfFile.canRead()) {
		
			PropertiesConfiguration config = new PropertiesConfiguration(alternateConfLocation);
	
			FileChangedReloadingStrategy rstrat = new FileChangedReloadingStrategy();
			// look for changed file every minute
			//rstrat.setRefreshDelay(60000);
			//rstrat.setRefreshDelay(1000);
			//config.setReloadingStrategy(rstrat);
			
			/*config.addConfigurationListener(new ConfigurationListener() {

				@Override
				public void configurationChanged(ConfigurationEvent event) {			
					System.out.println("CONF Changed: " +event.getPropertyName() + " - " + event.getPropertyValue() );
					
					if(event.getPropertyName().equals("textgridHost")) {
						//setTextgridHost(event.getPropertyValue().toString());
						setTgsearchConfigChanged(true);
					}
				}		
			});*/
			
			System.out.println("Config override found at: " + alternateConfLocation);
			System.out.println("Using  host: " + config.getString("textgridHost"));
			setTextgridHost(config.getString("textgridHost"));

		} else {
			System.out.println("No config override found at: " + alternateConfLocation);
			System.out.println("Using defaults, host: " + this.textgridHost);
		}
	}
/*
	public boolean isTgsearchConfigChanged() {
		return tgsearchConfigChanged;
	}

	public void setTgsearchConfigChanged(boolean tgsearchConfigChanged) {
		this.tgsearchConfigChanged = tgsearchConfigChanged;
	}
*/
}
