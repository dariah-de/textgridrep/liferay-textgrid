/*
 * #%L
 * TextGrid :: TGSearch :: Client
 * %%
 * Copyright (C) 2011 - 2012 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen (http://www.sub.uni-goettingen.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.service.tgsearch;

import info.textgrid.middleware.tgsearch.api.Baseline;
import info.textgrid.middleware.tgsearch.api.FacetQuery;
import info.textgrid.middleware.tgsearch.api.Info;
import info.textgrid.middleware.tgsearch.api.Navigation;
import info.textgrid.middleware.tgsearch.api.Relation;
import info.textgrid.middleware.tgsearch.api.Search;
import info.textgrid.middleware.tgsearch.api.TGSearchConstants;
import info.textgrid.namespaces.middleware.tgsearch.FacetResponse;
import info.textgrid.namespaces.middleware.tgsearch.PathResponse;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.Revisions;
import info.textgrid.namespaces.middleware.tgsearch.TextgridUris;

import java.util.Arrays;
import java.util.List;

import org.apache.cxf.jaxrs.client.ClientConfiguration;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.transport.common.gzip.GZIPInInterceptor;
import org.apache.cxf.transport.common.gzip.GZIPOutInterceptor;

public class SearchClient {
	
	private String sid = "";
	private String logstring = "";
	private int limit = TGSearchConstants.SEARCH_CLIENT_ITEM_LIMIT;
	private String order = "asc:title";
	private String target = "both";
	private int kwicWidth = TGSearchConstants.KWIC_WIDTH_LIMIT;
	private int wordDistance =-1;
	private boolean resolvePath = false;

	private Search search;
	private Navigation nav;
	private Info info;
	private Relation relation;
	private Baseline baseline;
	private FacetQuery facetquery;
	private List<String> facets;
	private static final String BOOLEAN_FALSE = "false"; 
	private static final String BOOLEAN_TRUE = "true";
	//private boolean sandbox = false;

	
	/**
	 * Constructor, sets up tgsearch http client for provided endpoint
	 * 
	 * @param endpoint	
	 * 				http adress of tgsearch
	 */
	public SearchClient(String endpoint) {
		
		facets = Arrays.asList(new String[] {"edition.agent.value", "work.genre", "format", "project.value"});
		
		/*ClientBuilder.newBuilder();
		Client client = ClientBuilder.newClient();
		
		WebTarget searchTarget = client.target(endpoint);
		searchTarget.path("search");
		Invocation.Builder builder = searchTarget.request();
		this.search = builder.get(Search.class);
		
		WebTarget navigationTarget = client.target(endpoint);
		navigationTarget.path("navigation");
		Invocation.Builder navigationBuilder = navigationTarget.request();
		this.nav = navigationBuilder.get(Navigation.class);
		
		WebTarget infoTarget = client.target(endpoint);
		infoTarget.path("navigation");
		Invocation.Builder infoBuilder = navigationTarget.request();
		this.info = infoBuilder.get(Info.class);
		
		WebTarget relationTarget = client.target(endpoint);
		relationTarget.path("navigation");
		Invocation.Builder relationBuilder = navigationTarget.request();
		this.relation = relationBuilder.get(Relation.class);
		
		WebTarget baselineTarget = client.target(endpoint);
		baselineTarget.path("navigation");
		Invocation.Builder baselineBuilder = navigationTarget.request();
		this.baseline = baselineBuilder.get(Baseline.class);*/
		
		this.search = JAXRSClientFactory.create(endpoint + "/search", Search.class);
		this.nav = JAXRSClientFactory.create(endpoint + "/navigation", Navigation.class);
		this.info = JAXRSClientFactory.create(endpoint + "/info", Info.class);
		this.relation = JAXRSClientFactory.create(endpoint + "/relation", Relation.class);
		this.baseline = JAXRSClientFactory.create(endpoint + "/baseline", Baseline.class);
		this.facetquery = JAXRSClientFactory.create(endpoint + "/facet", FacetQuery.class);
		
		//ClientConfiguration config = WebClient.getConfig(this.search);
		//HTTPConduit conduit1 = (HTTPConduit)config.getConduit();
		/*JAXRSClientFactoryBean bean = new JAXRSClientFactoryBean();
		bean.setAddress(endpoint + "/search");
		bean.setResourceClass(Search.class);
		
		GZIPFeature gf = new GZIPFeature();
		List<AbstractFeature> features = new ArrayList<AbstractFeature>();
		features.add((AbstractFeature)gf);
		
		bean.setFeatures(features);
		
		this.search = bean.create(Search.class);
*/		//Search t = bean.create(Search.class);
//		WebClient.client(this.search).header("Accept-Encoding", "gzip,deflate");
		//WebClient.client(this.search).header("Content-Encoding", "gzip");

	}
	
	public void enableGzipCompression() {
		ClientConfiguration sconfig = WebClient.getConfig(this.search);
		sconfig.getInInterceptors().add(new GZIPInInterceptor()); 
		sconfig.getOutInterceptors().add(new GZIPOutInterceptor());
		
		ClientConfiguration nconfig = WebClient.getConfig(this.nav);
		nconfig.getInInterceptors().add(new GZIPInInterceptor()); 
		nconfig.getOutInterceptors().add(new GZIPOutInterceptor());
		
		ClientConfiguration iconfig = WebClient.getConfig(this.info);
		iconfig.getInInterceptors().add(new GZIPInInterceptor()); 
		iconfig.getOutInterceptors().add(new GZIPOutInterceptor());
		
		ClientConfiguration rconfig = WebClient.getConfig(this.relation);
		rconfig.getInInterceptors().add(new GZIPInInterceptor()); 
		rconfig.getOutInterceptors().add(new GZIPOutInterceptor());
		
		ClientConfiguration bconfig = WebClient.getConfig(this.baseline);
		bconfig.getInInterceptors().add(new GZIPInInterceptor()); 
		bconfig.getOutInterceptors().add(new GZIPOutInterceptor());
	}
	
	public Response filter(List<String> filter, String order, int start, int limit, boolean sandbox) {
		String pstring = Boolean.valueOf(resolvePath).toString();
		return search.getQuery("*", target, sid, logstring, "", order, start, limit, kwicWidth, wordDistance, pstring, BOOLEAN_FALSE, Boolean.toString(sandbox), filter, null, 10);
	}

	/**
	 * Query tgsearch for metadata or fulltext. 
	 * Starts with 0, uses defaults for LIMIT and ORDER
	 * 
	 * TODO: link to tgsearch-querlanguage-explanation needs to be added to javadoc. 
	 * 
	 * @param query	
	 * 				string with query for tgsearch
	 * @return
	 * 				tgsearch response object containing results
	 */
	public Response query(String query, boolean sandbox) {
		   int start = 0;
		   String pstring = Boolean.valueOf(resolvePath).toString();
		   //String pstring = new Boolean(resolvePath).toString();
		   

		   
	       return search.getQuery(query, target, sid, logstring, "", order, start, limit, kwicWidth, wordDistance, pstring, BOOLEAN_FALSE, Boolean.toString(sandbox), null, facets, 10);	              
	}
	
	
	/**
	 * Query tgsearch for metadata or fulltext.
	 * 
	 * @param query
	 * 				string with query for tgsearch
	 * @param order
	 * 				sort-order like "asc:title" or "desc:agent"
	 * @param sessionid
	 * 				tgsearch-sessionid
	 * @param start
	 * 				start with
	 * @param limit
	 * 				max results to return
	 * @return search
	 */
	public Response query(String query, String order, String sessionid, int start, int limit, boolean sandbox) {
		   String pstring = Boolean.valueOf(resolvePath).toString();
	       return search.getQuery(query, target, sid, logstring, sessionid, order, start, limit, kwicWidth, wordDistance, pstring, BOOLEAN_FALSE, Boolean.toString(sandbox), null, facets, 0);	       	       
	}
	
	public Response query(String query, String order, String sessionid, int start, int limit, List<String> filter, boolean sandbox) {
		   String pstring = Boolean.valueOf(resolvePath).toString();
	       return search.getQuery(query, target, sid, logstring, sessionid, order, start, limit, kwicWidth, wordDistance, pstring, BOOLEAN_FALSE, Boolean.toString(sandbox), filter, facets, 0);	       	       
	}	

	/**
	 * Query tgsearch for metadata or fulltext in all projects, possibly slow, if many hits found.
	 * 
	 * @param query
	 * 				string with query for tgsearch
	 * @param order
	 * 				sort-order like "asc:title" or "desc:agent"
	 * @param sessionid
	 * 				tgsearch-sessionid
	 * @param start
	 * 				start with
	 * @param limit
	 * 				max results to return
	 * @return search
	 */
	public Response queryAllProjects(String query, String order, String sessionid, int start, int limit, boolean sandbox) {
		   String pstring = Boolean.valueOf(resolvePath).toString();
	       return search.getQuery(query, target, sid, logstring, sessionid, order, start, limit, kwicWidth, wordDistance, pstring, BOOLEAN_TRUE, Boolean.toString(sandbox), null, facets, 10);	              
	}
	
	/**
	 * Query baseline xml
	 * 
	 * @param query
	 * @param order
	 * @param sessionid  not used yet ;-)
	 * @param start
	 * @param limit
	 * @return baseline
	 */
	public Response queryBaseline(String query, String order, String sessionid, int start, int limit) {
		return baseline.getQuery(query, sid, order, logstring, start, limit);
	}	
	
	/**
	 * Query baseline xml
	 * 
	 * @param query
	 * @param start
	 * @return baseline
	 */
	public Response queryBaseline(String query, int start) {
		return baseline.getQuery(query, sid, order, logstring, start, limit);
	}
	
	/**
	 * List contents of a textgridproject
	 * 
	 * @param id	
	 * 			id of textgridproject
	 * @return
	 * 			tgsearch response object containing results
	 */
	public Response listProject(String id) {
	       return nav.listProject(id, sid, logstring);	       	 
	}
	
	/**
	 * List contents of an aggregation
	 * 
	 * @param id
	 * 			uri of aggregation
	 * @return
	 * 			tgsearch response
	 */
	public Response listAggregation(String id) {
	       return nav.listAggregation(id, sid, logstring);	
	}
	
	/**
	 * List toplevel aggregations from public textgrid repository
	 * 
	 * @return
	 * 			tgsearch response
	 */
	public Response listToplevelAggregations() {
	       return nav.listToplevelAggregations();
	}	
	
	/**
	 * List revisions of a textgrid object
	 * 
	 * @param uri
	 * 			uri of textgrid object
	 * @return
	 * 			revisions of object
	 */
	public Revisions listRevisions(String uri) {
		return info.revisions(uri);
	}
	
	/**
	 * List revisions of a textgrid object, include metadata
	 * 
	 * @param uri
	 * 			uri of textgrid object
	 * @return
	 * 			revisions of object
	 */
	public Response listRevisionsAndMeta(String uri) {
		return info.revisionsAndMeta(uri, sid);
	}	
	
	/**
	 * Retrieve metadata for a textgrid object specified by uri
	 * 
	 * @param uri
	 * 			uri of textgrid object
	 * @return
	 * 			metadata for uri
	 */
	public Response getMetadata(String uri) {
		String pstring = Boolean.valueOf(resolvePath).toString();
		return info.metadata(uri, sid, pstring);
	}
	
	public Response getMetadataAndPath(String uri) {
		String pstring = Boolean.valueOf(true).toString();
		return info.metadata(uri, sid, pstring);
	}	
	
	/**
	 * Find related objects, return relations as
	 * SPO (subject predicate object)
	 * 
	 * @param relation
	 * 			the relation to find
	 * @param uri
	 * 			uri of object
	 * @return
	 * 			list of relations
	 */
	public Response getRelated(String relation, String uri) {
		return this.relation.getRelation(sid, logstring, relation, uri);
	}
	
	/**
	 * Find related objects, return only metadata for related objects,
	 * may contain empty nodes if not authorized
	 * 
	 * @param relation
	 * 			the relation to find
	 * @param uri
	 * 			uri of object
	 * @return
	 * 			metadata of relateded objects 
	 */
	public Response getMeta4Relation(String relation, String uri) {
		String pstring = Boolean.valueOf(resolvePath).toString();
		return this.relation.getOnlyMetadata(sid, logstring, relation, uri, pstring);
	}
	
	/**
     * Find related objects, return metadata for related objects and
     * relations as SPO (subject predicate object)
	 * may contain empty nodes if not authorized.
	 * 
	 * @param relation
	 * 			the relation to find
	 * @param uri
	 * 			uri of object
	 * @return
	 * 			metadata and relations
	 */
	public Response getRelatedAndMeta(String relation, String uri) {
		return this.relation.getCombined(sid, logstring, relation, uri);
	}
	
	/**
	 * Find Path to Edition and Work for given uri.
	 * 
	 * @param uri
	 * @return uri
	 */
	public PathResponse getPath(String uri) {
		return this.info.getPath(uri);
	}
	
	/**
	 * Get related Metadata for the Object. 
	 * Also get Metadata for related Edition and Work. 
	 * @param uri
	 * @return uri and sid
	 */
	public Response getEditionWorkMeta(String uri) {
		return this.info.getEditionWorkMeta(uri, sid);
	}
	
	/**
	 * find parent aggregations for a given uri,
	 * searches all the way to the root
	 * @param uri
	 * @return
	 */
	public TextgridUris getParents(String uri) {
		return this.info.getParents(uri);
	}
	
	/**
	 * find children for given aggregation uri, 
	 * this is a deep search on the whole tree 
	 * @param uri
	 * @return
	 */
	public TextgridUris getChildren(String uri) {
		return this.info.getChildren(uri);
	}
	
	/**
	 * get facets for textgrid metadata fields
	 * @param facetList		metadata fields to get facets for
	 * @return
	 */
	public FacetResponse getFacets(List<String> facetList, int limit, String order, boolean sandbox) {
		return this.facetquery.facetQuery(facetList, limit, order, Boolean.toString(sandbox));
	}

	/**
	 * get facets for textgrid metadata fields, for a specific query
	 * @param facetList		metadata fields to get facets for
	 * @param order			order by count or term ascending (asc) or descending, (desc) e.g. 
	 * 						setting the string "term:asc". defaults to "count:desc"
	 * @return
	 */
/*	public FacetResponse getFacets(List<String> facetList, String order) {
		return this.facetquery.facetQuery(facetList, 10, order);
	}*/
	
	/**
	 * Find Path to Edition and Work for given uri.
	 * 
	 * @param uri
	 * @return uri, sid and logstring
	 */
	/*
	public Response getPathAndMeta(String uri) {
		return this.nav.getPathMeta(uri, sid, logstring);
	}	
    */
	/**
	 * Set tgauth sessionid to be used for querying tgsearch
	 * 
	 * @param sid
	 * 			tgauth sessionid
	 */
	public void setSid(String sid) {
		this.sid = sid;
	}

	/**
	 * Get currently set sessionid
	 * 
	 * @return
	 * 			tgauth sessionid
	 */
	public String getSid() {
		return sid;
	}

	/**
	 * Set textgridlog - logstring for beeing sent with tgsearch-requests
	 * 
	 * @param logstring
	 * 			tglog logstring
	 */
	public void setLogstring(String logstring) {
		this.logstring = logstring;
	}

	/**
	 * Get logstring
	 * 
	 * @return
	 * 		tglog logstring
	 */
	public String getLogstring() {
		return logstring;
	}

	/**
	 * Set default limit of SearchClient-Object
	 * Default is 20
	 * 
	 * @param limit
	 */
	public void setLIMIT(int limit) {
		this.limit = limit;
	}

	/**
	 * Set default order of SearchClient-Object
	 * Default is asc:title
	 * 
	 * @param order
	 */
	public void setORDER(String order) {
		this.order = order;
	}
	
	/**
	 * get target for fulltext search
	 * @return target
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * set target for fulltext search, 
	 * one of "structure", "metadata" and "both", defaults to "both"
	 * 
	 * @param target
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * how much chars to return before and after every kwic-hit
	 * 
	 * @param kwicWidth
	 */
	public void setKwicWidth(int kwicWidth) {
		this.kwicWidth = kwicWidth;
	}

	/**
	 * how much chars to return before and after every kwic-hit
	 * 
	 * @param kwicWidth
	 */
	public int getKwicWidth() {
		return kwicWidth;
	}	

	/**
	 * max distance beetween two words in fulltext query. 
	 * ignored if set to a number < 0. then for a hit all words 
	 * must be contained in one document. defaults to -1
	 * 
	 * @param wordDistance
	 */
	public void setWordDistance(int wordDistance) {
		this.wordDistance = wordDistance;
	}
	
	public int getWordDistance() {
		return wordDistance;
	}

	/**
	 * search results should also contain path, 
	 * e.g. item -> edition -> work
	 * 
	 * defaults to false
	 * 
	 * @param getPath
	 */
	public void setResolvePath(boolean getPath) {
		this.resolvePath = getPath;
	}

	public boolean isResolvePath() {
		return resolvePath;
	}
	
}
