package info.textgrid.service.tgsearch;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

@Service
public class AggregatorClientService {
	
	private static final Log log = LogFactoryUtil.getLog(AggregatorClientService.class);
	private TgrepConfigurationService tgrepConfig;
	
	Client restClient = ClientBuilder
			.newClient().property("thread.safe.client", "true");

	@Autowired
	public AggregatorClientService(TgrepConfigurationService tgrepConfig) {
		this.tgrepConfig = tgrepConfig;
		
		
	}

	private static final long serialVersionUID = -2920076918342185011L;
	
	public String renderTEI(String id) {
		
		WebTarget target = restClient.target(this.tgrepConfig.getTextgridHost());
		target = target.path("/1.0/aggregator/html/"+id)
				.queryParam("embedded", "true")
				.queryParam("mediatype", "text/xml");
		 
		Invocation.Builder builder = target.request();

		Response result = builder.get();
		return result.readEntity(String.class);
		
	}
	
	public String renderFromQuery(String query, List<String> filter, String format) {
		
		WebTarget target = restClient.target(this.tgrepConfig.getTextgridHost());
		target = target.path("/1.0/aggregator/html/"+format+"/query")
				.queryParam("query", query)
				.queryParam("filter", filter);
		 
		Invocation.Builder builder = target.request();	
		return builder.get(String.class);
	}
	
	public String renderToc(String id) {
		
		String result="";

		WebTarget target = restClient.target(this.tgrepConfig.getTextgridHost());
		target = target.path("/1.0/aggregator/html/"+id)
				.queryParam("toc", "true")
				.queryParam("mediatype", "text/xml");

		try {
			Invocation.Builder builder = target.request();
			result = builder.get(String.class);
		} catch (javax.ws.rs.InternalServerErrorException e) {
			log.info("Error from AggregatorService", e);
		}

		return result;

	}	
	
	public String renderTEIFragment(String id, String fragmentId) {
		
		String linkPattern = "";
		try {
			linkPattern = URLEncoder.encode("/browse/_/tgrep/@URI@&fragment=@ID@", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//System.out.println("lp: " + linkPattern);
		
		WebTarget target = restClient.target(this.tgrepConfig.getTextgridHost());
		target = target.path("/1.0/aggregator/html/"+id)
				.queryParam("embedded", "true")
				.queryParam("mediatype", "text/xml")
				.queryParam("id", fragmentId)
				.queryParam("linkPattern", linkPattern);
		 
		Invocation.Builder builder = target.request();
		//Response response = builder.get();
		
		//System.out.println("response is there: " + response.getStatus());
		
		return builder.get(String.class);
	}	
	
	

}