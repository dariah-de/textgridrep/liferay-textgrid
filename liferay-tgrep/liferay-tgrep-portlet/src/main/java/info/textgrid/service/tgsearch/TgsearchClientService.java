package info.textgrid.service.tgsearch;

import info.textgrid.namespaces.middleware.tgsearch.FacetResponse;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.event.ConfigurationEvent;
import org.apache.commons.configuration.event.ConfigurationListener;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TgsearchClientService {
	
	private SearchClient sc;
	private TgrepConfigurationService tgrepConfig;

	
	//private int LIMIT = 5;
	
	@Autowired
	public TgsearchClientService(TgrepConfigurationService tgrepConfig) {
		this.tgrepConfig = tgrepConfig;
	}
	
	@PostConstruct
	public void postConstruct() {
		this.setupClient();
	}
	
	private void setupClient() {
		System.out.println("setting up tgsearch client for host: " + tgrepConfig.getTextgridHost());
		sc = new SearchClient(tgrepConfig.getTextgridHost() + "/1.0/tgsearch-public");
		//sc.enableGzipCompression();
		sc.setResolvePath(true);		
	}
	
	public Response search(String query, String order, int start, int limit, List<String> filter, boolean sandbox) {
		//System.out.println("\n=======\n query for " + query + "\n==========\n");
		//this.checkConfigChange();
		return sc.query(query, order, "", start, limit, filter, sandbox);
	}
	
	public Response filter(List<String> filter, int start, int limit, boolean sandbox) {
		//System.out.println("\n=======\n filter for " + filter + "\n==========\n");
		//this.checkConfigChange();
		return sc.filter(filter, "title:asc", start, limit, sandbox);
	}
	
	public Response listRootCollections() {
		//this.checkConfigChange();
		return sc.listToplevelAggregations();
	}
	
	public Response listAggregation(String id) {
		//this.checkConfigChange();
		return sc.listAggregation(id);
	}
	
	public FacetResponse listFacet(List<String> facetList, int limit, String order, boolean sandbox) {
		//this.checkConfigChange();
		return sc.getFacets(facetList, limit, order, sandbox);
	}
	
	public ResultType getMetadata(String id) {
		//this.checkConfigChange();
		ResultType rs = sc.getMetadataAndPath(id).getResult().get(0);
		return rs;
	}
	
/*
	private void checkConfigChange() {
		System.out.println("check for confchange");
		if(this.tgrepConfig.isTgsearchConfigChanged()) {
			System.out.println("tgsearch config changed");
			this.setupClient();
			this.tgrepConfig.setTgsearchConfigChanged(false);
		}
	}
*/

}
