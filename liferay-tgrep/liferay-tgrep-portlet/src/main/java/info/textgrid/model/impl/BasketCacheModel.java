package info.textgrid.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.textgrid.model.Basket;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Basket in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Basket
 * @generated
 */
public class BasketCacheModel implements CacheModel<Basket>, Externalizable {
    public String uuid;
    public long basketId;
    public long userId;
    public String textgridUri;
    public int order;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{uuid=");
        sb.append(uuid);
        sb.append(", basketId=");
        sb.append(basketId);
        sb.append(", userId=");
        sb.append(userId);
        sb.append(", textgridUri=");
        sb.append(textgridUri);
        sb.append(", order=");
        sb.append(order);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Basket toEntityModel() {
        BasketImpl basketImpl = new BasketImpl();

        if (uuid == null) {
            basketImpl.setUuid(StringPool.BLANK);
        } else {
            basketImpl.setUuid(uuid);
        }

        basketImpl.setBasketId(basketId);
        basketImpl.setUserId(userId);

        if (textgridUri == null) {
            basketImpl.setTextgridUri(StringPool.BLANK);
        } else {
            basketImpl.setTextgridUri(textgridUri);
        }

        basketImpl.setOrder(order);

        basketImpl.resetOriginalValues();

        return basketImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        uuid = objectInput.readUTF();
        basketId = objectInput.readLong();
        userId = objectInput.readLong();
        textgridUri = objectInput.readUTF();
        order = objectInput.readInt();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (uuid == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(uuid);
        }

        objectOutput.writeLong(basketId);
        objectOutput.writeLong(userId);

        if (textgridUri == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(textgridUri);
        }

        objectOutput.writeInt(order);
    }
}
