package info.textgrid.model.impl;

/**
 * The extended model implementation for the Basket service. Represents a row in the &quot;textgridrep_Basket&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.textgrid.model.Basket} interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
public class BasketImpl extends BasketBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a basket model instance should use the {@link info.textgrid.model.Basket} interface instead.
     */
    public BasketImpl() {
    }
}
