<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<form class="tgrep advanced-search">

	<h1>Erweiterte Suche</h1>
	<fieldset class="tgrep advanced-search_fieldset"><legend class="tgrep advanced-search_legend">Metadata</legend>
	
		<fieldset class="tgrep advanced-search_fieldset">
			<ul class="tgrep advanced-search_grow-group">
				<li class="tgrep advanced-search_grow-item metadata-searchgroup">
					<span class="tgrep advanced-search_input-group -operator -block">AND </span> 
					<label class="tgrep advanced-search_input-group -type"> 
						<span class="sr-only">Field name</span> 
						<select class="tgrep advanced-search_input -full-width" name="metadata-field">
							<option value="title">Title</option>
							<option value="edition.agent.value">Author</option>
							<option value="edition.language">Language</option>
							<option value="notes">Notes</option>
							<option value="genre">Genre</option>
							<option value="rightsHolder">Rights Holder</option>
							<option value="format">Format</option>
							<option value="work.dateOfCreation.date">Date</option>
							<option value="work.dateOfCreation.notBefore">Date (not Before)</option>
							<option value="work.dateOfCreation.noAfter">Date (note After)</option>
						 </select>
					 </label> 
				 	<label class="tgrep advanced-search_input-group -term"> 
				 		<span class="sr-only">Search term</span> 
				 		<input class="tgrep advanced-search_input -full-width" name="metadata-term" type="text" />
			 		</label> 
			 		<span class="tgrep advanced-search_input-group -operator"> OR </span> 
			 		<label class="tgrep advanced-search_input-group -term"> 
			 			<span class="sr-only">Search term</span> 
			 			<input class="tgrep advanced-search_input -full-width" name="metadata-term" type="text" /> 
			 		</label> 
			 		<span class="tgrep advanced-search_input-group -buttons">
				 		<button class="tgrep advanced-search_button -item -add" type="button">
				 			<span class="sr-only">Add another field</span>
			 			</button>
				 		<button class="tgrep advanced-search_button -item -remove" disabled="disabled" type="button">
				 			<span class="sr-only">Remove this field</span>
				 		</button> 
		 			</span>
	 			</li>
			</ul>
		</fieldset>
		
		<button class="tgrep advanced-search_button -toggle" data-target="content-types" type="button">Content Types</button>
		
		<fieldset class="tgrep advanced-search_fieldset -toggle" id="content-types">
		
			<p class="tgrep advanced-search_input-group -buttons">
				<button class="tgrep advanced-search_button -select -all" data-target="content-types" type="button">Alle markieren</button>
				<button class="tgrep advanced-search_button -select -none" data-target="content-types" type="button">Alle abwählen</button>
			</p>
			
			<ul>
				<li><input id="content-type-0" name="contenttype" type="checkbox" value="text/tg.aggregation+xml" /> <label for="content-type-0">Aggregation (text/tg.aggregation+xml)</label></li>
				<li><input id="content-type-1" name="contenttype" type="checkbox" value="text/css" /> <label for="content-type-1">Cascading Stylesheet (CSS) (text/css)</label></li>
				<li><input id="content-type-2" name="contenttype" type="checkbox" value="text/tg.collection+tg.aggregation+xml" /> <label for="content-type-2">Collection (text/tg.collection+tg.aggregation+xml)</label></li>
				<li><input id="content-type-3" name="contenttype" type="checkbox" value="application/xml-dtd" /> <label for="content-type-3">DTD (application/xml-dtd)</label></li>
				<li><input id="content-type-4" name="contenttype" type="checkbox" value="text/tg.edition+tg.aggregation+xml" /> <label for="content-type-4">Edition (text/tg.edition+tg.aggregation+xml)</label></li>
				<li><input id="content-type-5" name="contenttype" type="checkbox" value="image/x-unknown" /> <label for="content-type-5">Image (image/x-unknown)</label></li>
				<li><input id="content-type-6" name="contenttype" type="checkbox" value="text/tg.imex+xml" /> <label for="content-type-6">Import / Export Mapping (text/tg.imex+xml)</label></li>
				<li><input id="content-type-7" name="contenttype" type="checkbox" value="image/jpeg" /> <label for="content-type-7">JPEG image (image/jpeg)</label></li>
				<li><input id="content-type-8" name="contenttype" type="checkbox" value="image/png" /> <label for="content-type-8">PNG image (image/png)</label></li>
				<li><input id="content-type-9" name="contenttype" type="checkbox" value="text/plain" /> <label for="content-type-9">Plain Text (text/plain)</label></li>
				<li><input id="content-type-10" name="contenttype" type="checkbox" value="text/tg.projectfile+xml" /> <label for="content-type-10">Project Configuration (text/tg.projectfile+xml)</label></li>
				<li><input id="content-type-11" name="contenttype" type="checkbox" value="image/tiff" /> <label for="content-type-11">TIFF image (image/tiff)</label></li>
				<li><input id="content-type-12" name="contenttype" type="checkbox" value="text/linkeditorlinkedfile" /> <label for="content-type-12">Text Image Link File (text/linkeditorlinkedfile)</label></li>
				<li><input id="content-type-13" name="contenttype" type="checkbox" value="text/tg.servicedescription+xml" /> <label for="content-type-13">TextGrid Service Description (text/tg.servicedescription+xml)</label></li>
				<li><input id="content-type-14" name="contenttype" type="checkbox" value="text/tg.inputform+rdf+xml" /> <label for="content-type-14">TextGrid TGForm RDF-Object (text/tg.inputform+rdf+xml)</label></li>
				<li><input id="content-type-15" name="contenttype" type="checkbox" value="text/gwdl.workflow+xml" /> <label for="content-type-15">TextGrid Workflow Document (GWDL) (text/gwdl.workflow+xml)</label></li>
				<li><input id="content-type-16" name="contenttype" type="checkbox" value="text/tg.workflow+xml" /> <label for="content-type-16">TextGrid Workflow Document (text/tg.workflow+xml)</label></li>
				<li><input id="content-type-17" name="contenttype" type="checkbox" value="text/tg.work+xml" /> <label for="content-type-17">Work (text/tg.work+xml)</label></li>
				<li><input id="content-type-18" name="contenttype" type="checkbox" value="text/xml" /> <label for="content-type-18">XML Document (text/xml)</label></li>
				<li><input id="content-type-19" name="contenttype" type="checkbox" value="text/xsd+xml" /> <label for="content-type-19">XML Schema (text/xsd+xml)</label></li>
				<li><input id="content-type-20" name="contenttype" type="checkbox" value="text/xml+xslt" /> <label for="content-type-20">XSLT Stylesheet (text/xml+xslt)</label></li>
				<li><input id="content-type-21" name="contenttype" type="checkbox" value="application/zip" /> <label for="content-type-21">ZIP Archive (application/zip)</label></li>
				<li><input id="content-type-22" name="contenttype" type="checkbox" value="text/tg.webpublisher+xml" /> <label for="content-type-22">webpublisher (text/tg.webpublisher+xml)</label></li>
			</ul>
		</fieldset>
	</fieldset>
	
	<fieldset class="tgrep advanced-search_fieldset"><legend class="tgrep advanced-search_legend">Fulltext</legend>
	
		<fieldset class="tgrep advanced-search_fieldset">
			<label class="sr-only" for="advanced-search_fulltext-input">Search terms</label> 
			<input class="tgrep advanced-search_input -full-width" id="advanced-search_fulltext-input" name="fulltext" type="text" />
		</fieldset>
			
		<button class="tgrep advanced-search_button -toggle" data-target="word-distance" type="button">Word distances</button>
			
		<fieldset class="tgrep advanced-search_fieldset -toggle" id="word-distance">
			<ul>
				<li><input checked="checked" id="word-distance-0" name="wordDistanceSel" type="radio" value="false" /> <label for="word-distance-0"> in one document </label></li>
				<li><input id="word-distance-1" name="wordDistanceSel" type="radio" value="true" /> <label for="word-distance-1"> no more than <input max="1000" min="1" name="wordDistance" step="1" type="number" value="40" /> words </label></li>
			</ul>
		</fieldset>
		
	</fieldset>
	
	<input class="tgrep advanced-search_submit" type="submit" value="Search" />

</form>
