<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://textgrid.info/repportlet/utils" prefix="utils" %>

<portlet:defineObjects />

<portlet:resourceURL id="addToBasket" var="addToBasketUrl" ></portlet:resourceURL>
<portlet:resourceURL id="removeFromBasket" var="removeFromBasketUrl" ></portlet:resourceURL>

<%@ include file="components/js-incl.jsp" %>

<script type="text/javascript">

	$(function() {
		// set order-select
		$(".tgrep.header_order-by option[value='${order}']").attr('selected',true);

		// set limit select
		$(".tgrep.header_limit option[value='${limit}']").attr('selected',true);
	});

	$(document).on('change', '.tgrep.header_order-by', function(e) {
		window.location.href='?query=${query}${filterQueryString}&start=${start}&limit=${limit}&order='+$(this).val();
	});

	$(document).on('change', '.tgrep.header_limit', function(e) {
		window.location.href='?query=${query}${filterQueryString}&order=${order}&start=${start}&limit='+$(this).val();
	});

</script>


<div class="tgrep wrap">

	<main class="tgrep main -full-width">

		<div class="tgrep browse">
				<ul class="tgrep browse_list">
					<c:forEach items="${facets}" var="result">
						<li class="tgrep browse_item">
							<c:url context="/" value="/search" var="encodedUrl">
							  <c:param name="filter" value="${facet}:${result.value}" />
							</c:url>
							<a class="tgrep browse_link" href="${encodedUrl}">
								${result.value}
							</a>
							(${result.count} items)
						</li>
					</c:forEach>
				</ul>
			</div>

	</main>

</div>
