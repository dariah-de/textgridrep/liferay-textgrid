<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<script>

document.addEventListener("DOMContentLoaded", function(event) {

	setBasketCount(${basketcount});

});

function setBasketCount(num) {
	[].forEach.call(document.getElementsByClassName("topbox_shelf-count"), function(elem){
		elem.textContent = num;
	})
}

</script>
