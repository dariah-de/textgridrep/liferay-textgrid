<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<portlet:defineObjects />
<portlet:actionURL var="submitFormURL" name="handleUserSettings"/>

<div class="tgrep wrap">

	<main class="tgrep main">

		<form:form name="userSettings" method="post" modelAttribute="userSettings" action="<%=submitFormURL.toString() %>">

			<fieldset>
				<legend>${i18n['usersettings']}</legend>
				<div>
					${i18n['show-results-from-sandbox']}: &nbsp;&nbsp;<form:checkbox style="visibility:visible" name="sandboxEnabled" path="sandboxEnabled" value="sandboxEnabled" />
				</div>
				<!-- 
				<div>
					${i18n['annotation-enabled']}: &nbsp;&nbsp;<form:checkbox style="visibility:visible" name="annotationEnabled" path="annotationEnabled" value="annotationEnabled" />
				</div>
				-->
				<button type="submit">${i18n['save']}</button>

			</fieldset>

		</form:form>

	</main>

</div>
