<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://textgrid.info/repportlet/utils" prefix="utils" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<portlet:defineObjects />

<portlet:resourceURL id="addToBasket" var="addToBasketUrl" ></portlet:resourceURL>
<portlet:resourceURL id="removeFromBasket" var="removeFromBasketUrl" ></portlet:resourceURL>

<script type="text/javascript">

document.addEventListener("DOMContentLoaded", function(event) {

	function addFiltersToSearchForm(filters) {
		filters.forEach(function(filter) {
			$('form.search').append($('<input type="hidden" class="hiddenFilterInput" name="filter" value="'+filter+'" />'));
		});
	}

	function removeFiltersFromSearchForm() {
		$('.hiddenFilterInput').remove();
	}

	var filters = [];

	<c:forEach items="${filter}" var="activeFilter">
		filters.push('${activeFilter}');
	</c:forEach>

	addFiltersToSearchForm(filters);

	$('form.search').append($('<input type="hidden" name="order" value="${order}" />'));
	$('form.search').append($('<input type="hidden" name="limit" value="${limit}" />'));

	$('#search-filters-active').change(function() {
		if($('#search-filters-active').is(':checked')){
			addFiltersToSearchForm(filters);
		} else {
			removeFiltersFromSearchForm();
		}
	});

});

</script>

<%@ include file="components/js-incl.jsp" %>

<div class="tgrep wrap">

<c:if test="${results.size() eq 0 and not empty query and empty filter}">
	<main class="tgrep main">
		
		 ${i18n['nothing-found']} <em>${query}</em>

	</main>
</c:if>

<c:if test="${results.size() > 0 or not empty filter}">

	<div class="tgrep sidebar-toggle">
		<button class="tgrep sidebar-toggle_button -show">${i18n['show-sidebar']}</button>
		<button class="tgrep sidebar-toggle_button -hide">${i18n['hide-sidebar']}</button>
	</div>

	<aside class="tgrep sidebar -filter">
		<c:if test="${viewmodes != null}">
			<section class="tgrep sidebar_panel">
				<h3 class="tgrep sidebar_subheading">${i18n['views']}</h3>
				<ul class="tgrep sidebar_list">
					<c:forEach items="${viewmodes}" var="viewmode">
						<li class="tgrep sidebar_item ${viewmode.active? '-current' : ''}">
							<a href="${viewmode.url}" rel="noindex nofollow" class="tgrep sidebar_link">${viewmode.label}</a>
						</li>
					</c:forEach>
				</ul>
				<c:if test="${viewmodes.size() > 6}">
					<button class="tgrep sidebar_expand">${i18n['expand']}</button>
				</c:if>
			</section>
		</c:if>

		<h2 class="tgrep sidebar_heading">${i18n['filter']}</h2>

		<section class="tgrep sidebar_panel">
			<h3 class="tgrep sidebar_subheading">${i18n['active-filters']}</h3>
				<ul class="tgrep sidebar_list">
					<c:forEach items="${filter}" var="activeFilter">
						<c:set var="afArray" value="${fn:split(activeFilter, ':')}" />
						<li class="tgrep sidebar_item -filter">
							<a class="tgrep sidebar_remove-filter"
									title="${i18n['remove-filter']}"
									href="?query=${query}&order=${order}&limit=${limit}${fn:replace(filterQueryString, '&filter='.concat(afArray[0]).concat('%3A').concat(utils:urlencode(afArray[1])) , '')}">
								<span class="sr-only">${i18n['remove-filter']}</span>
							</a>
							<strong>${i18n[afArray[0]]}</strong>: ${afArray[1]} 
						</li>
					</c:forEach>
				</ul>
				<c:if test="${filter.size() > 6}">
					<button class="tgrep sidebar_expand">${i18n['expand']}</button>
				</c:if>
		</section>

		<c:forEach items="${facets}" var="facetGroup">
			<c:if test="${facetGroup.facet.size() != 0}">
				<section class="tgrep sidebar_panel">
					<h3 class="tgrep sidebar_subheading">${i18n[facetGroup.name]}</h3>
					<ul class="tgrep sidebar_list">
						<c:forEach items="${facetGroup.facet}" var="facet">
							<li class="tgrep sidebar_item">
								<c:set var="filtermatch" value="${facetGroup.name}:${facet.value}" />
								<c:choose>
									<c:when test="${not empty filter and filter.contains(filtermatch)}">
										${facet.value}
									</c:when>
									<c:otherwise>
										<a href="?query=${query}&order=${order}&limit=${limit}&filter=${facetGroup.name}:${utils:urlencode(facet.value)}${filterQueryString}"
										class="tgrep sidebar_link">${facet.value}</a>
									</c:otherwise>
								</c:choose>
								<span class="tgrep sidebar_count">${facet.count}</span>
							</li>
						</c:forEach>
					</ul>
					<c:if test="${facetGroup.facet.size() > 6}">
						<button class="tgrep sidebar_expand">${i18n['expand']}</button>
					</c:if>
				</section>
			</c:if>
		</c:forEach>
	</aside>

	<main class="tgrep main">
		<c:if test="${results.size() > 0}">
			<%@ include file="components/searchResults.jsp" %>
		</c:if>
		<c:if test="${results.size() eq 0}">
			No Result found within active filters, remove filters to get matches for "${query}"
		</c:if>
	</main>
</c:if>
</div>
