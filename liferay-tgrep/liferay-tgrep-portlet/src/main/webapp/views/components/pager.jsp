<div class="tg pagination">
	<ul class="tg pagination_list">
		<!-- previous button -->
		<c:if test="${pager.start eq 0}">
			<li class="tg pagination_item -disabled">
				<span class="tg pagination_link -first" title="${i18n['go-to-first-page']}">
					<span class="sr-only">${i18n['first-page']}</span>
				</span>
			</li>

			<li class="tg pagination_item -disabled">
				<span class="tg pagination_link -prev" title="${i18n['go-to-previous-page']}">
					<span class="sr-only">${i18n['previous-page']}</span>
				</span>
			</li>
		</c:if>

    	<c:if test="${pager.start gt 0}">
			<li class="tg pagination_item">
				<a class="tg pagination_link -first" href="?query=${query}${filterQueryString}&start=0&limit=${pager.limit}&order=${order}" title="${i18n['go-to-first-page']}">
					<span class="sr-only">${i18n['first-page']}</span>
				</a>
			</li>

			<li class="tg pagination_item">
				<a class="tg pagination_link -prev" href="?query=${query}${filterQueryString}&start=${pager.start - pager.limit}&limit=${pager.limit}&order=${order}" title="${i18n['go-to-previous-page']}">
					<span class="sr-only">${i18n['previous-page']}</span>
				</a>
			</li>
		</c:if>


		<!-- pages -->
		<c:forEach items="${pager.pages}" var="page">

			<c:if test="${(page - 1) * pager.limit != pager.start}">
				<li class="tg pagination_item">
					<a class="tg pagination_link" href="?query=${query}${filterQueryString}&start=${(page - 1) * pager.limit}&limit=${pager.limit}&order=${order}" title="${i18n['go-to-page']} ${page}">
						<span class="sr-only">${i18n['page']}</span> ${page}
					</a>
				</li>
			</c:if>

			<c:if test="${(page - 1) * pager.limit == pager.start}">
				<li class="tg pagination_item -current">
					<a class="tg pagination_link" href="?query=${query}${filterQueryString}&start=${(page - 1) * limit}&limit=${pager.limit}&order=${order}" title="${i18n['go-to-page']} ${page}">
						<span class="sr-only">${i18n['page']}</span> ${page}
					</a>
				</li>
			</c:if>

		</c:forEach>

		<!-- next button -->
		<c:if test="${(pager.start + pager.limit) lt pager.hits }">
			<li class="tg pagination_item">
				<a class="tg pagination_link -next" href="?query=${query}${filterQueryString}&start=${pager.start + pager.limit}&limit=${pager.limit}&order=${order}" title="${i18n['go-to-next-page']}">
					<span class="sr-only">${i18n['next-page']}</span>
				</a>
			</li>
			<li class="tg pagination_item">
				<a class="tg pagination_link -last" href="?query=${query}${filterQueryString}&start=${(pager.totalPages-1) * pager.limit}&limit=${pager.limit}&order=${order}" title="${i18n['go-to-last-page']}">
					<span class="sr-only">${i18n['last-page']}</span>
				</a>
			</li>
		</c:if>

		<c:if test="${(pager.start + pager.limit) ge pager.hits }">
			<li class="tg pagination_item -disabled">
				<span class="tg pagination_link -next" title="${i18n['go-to-next-page']}">
					<span class="sr-only">${i18n['next-page']}</span>
				</span>
			</li>
			<li class="tg pagination_item -disabled">
				<span class="tg pagination_link -last" title="${i18n['go-to-last-page']}">
					<span class="sr-only">${i18n['last-page']}</span>
				</span>
			</li>
		</c:if>

	</ul>
</div>
