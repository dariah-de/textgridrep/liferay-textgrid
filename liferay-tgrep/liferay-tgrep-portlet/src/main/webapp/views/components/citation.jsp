<!-- Citation things depending on format (Edition, Collection, Work, Item) -->
<c:choose>
    <c:when test="${format == 'text/tg.edition+tg.aggregation+xml'}">
        <dl>
            <dt>${i18n['edition-citation-heading']}</dt>
            <dd>TextGrid Repository (${fn:substring(metadata.object.generic.generated.issued, 0, 4)}). ${metadata.object.edition.agent[0].value}. ${metadata.object.generic.provided.title[0]}. ${metadata.object.generic.generated.project.value}. <a href="https://hdl.handle.net/${fn:substringAfter(metadata.object.generic.generated.pid[0].value, 'hdl:')}">https://hdl.handle.net/${fn:substringAfter(metadata.object.generic.generated.pid[0].value, 'hdl:')}</a></dd>
        </dl>
    </c:when>

    <c:when test="${format == 'text/tg.collection+tg.aggregation+xml'}">
        <dl>
            <dt>${i18n['collection-citation-heading']}</dt>
            <dd>TextGrid Repository (${fn:substring(metadata.object.generic.generated.issued, 0, 4)}). ${metadata.object.generic.provided.title[0]}. ${metadata.object.generic.generated.project.value}. ${metadata.object.collection.collector[0].value}. <a href="https://hdl.handle.net/${fn:substringAfter(metadata.object.generic.generated.pid[0].value, 'hdl:')}">https://hdl.handle.net/${fn:substringAfter(metadata.object.generic.generated.pid[0].value, 'hdl:')}</a></dd>
        </dl>
    </c:when>

    <c:when test="${format == 'text/tg.aggregation+xml'}">
        <dl>
            <dt>${i18n['aggregation-citation-heading']}</dt>
            <dd>TextGrid Repository (${fn:substring(metadata.object.generic.generated.issued, 0, 4)}).
            <c:forEach items="${result.pathResponse.pathGroup}" var="pathGroup">
                <c:forEach items="${pathGroup.path}" var="path">
                    <c:forEach items="${path.entry}" var="pathEntry">
                        ${pathEntry.title}.
                    </c:forEach>
                </c:forEach>
            </c:forEach>
            ${metadata.object.generic.provided.title[0]}. ${metadata.object.generic.generated.project.value}. ${metadata.object.item.rightsHolder[0].value}. <a href="https://hdl.handle.net/${fn:substringAfter(metadata.object.generic.generated.pid[0].value, 'hdl:')}">https://hdl.handle.net/${fn:substringAfter(metadata.object.generic.generated.pid[0].value, 'hdl:')}</a></dd>
        </dl>
    </c:when>

    <c:when test="${format == 'text/tg.work+xml'}">
        <dl>
            <dt>${i18n['work-citation-heading']}</dt>
            <dd>TextGrid Repository (${fn:substring(metadata.object.generic.generated.issued, 0, 4)}). ${metadata.object.work.agent[0].value}. ${metadata.object.generic.provided.title[0]}. ${metadata.object.generic.generated.project.value}. <a href="https://hdl.handle.net/${fn:substringAfter(metadata.object.generic.generated.pid[0].value, 'hdl:')}">https://hdl.handle.net/${fn:substringAfter(metadata.object.generic.generated.pid[0].value, 'hdl:')}</a></dd>
        </dl>
    </c:when>

    <c:otherwise>
        <dl>
            <dt>${i18n['item-citation-heading']}</dt>
            <dd>TextGrid Repository (${fn:substring(metadata.object.generic.generated.issued, 0, 4)}).
            <c:forEach items="${result.pathResponse.pathGroup}" var="pathGroup">
                <c:forEach items="${pathGroup.path}" var="path">
                    <c:forEach items="${path.entry}" var="pathEntry">
                        ${pathEntry.title}.
                    </c:forEach>
                </c:forEach>
            </c:forEach>
            ${metadata.object.generic.provided.title[0]}. ${metadata.object.generic.generated.project.value}. ${metadata.object.item.rightsHolder[0].value}. <a href="https://hdl.handle.net/${fn:substringAfter(metadata.object.generic.generated.pid[0].value, 'hdl:')}">https://hdl.handle.net/${fn:substringAfter(metadata.object.generic.generated.pid[0].value, 'hdl:')}</a></dd>
        </dl>
    </c:otherwise>
</c:choose>
