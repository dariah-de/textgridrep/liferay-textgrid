

<header class="tgrep header">

	<div class="tgrep header_info">
		${i18n['displaying-results']} <span class="tgrep header_count -current">${pager.start + 1}&#8211;${pager.start + pager.limit}</span> ${i18n['of']}
		<span class="tgrep header_count -total">${pager.hits}</span>

		<div class="tg dropdown" role="group">
			<a class="tg dropdown_toggle -settings">${i18n['change-result-display']}</a>
			<ul class="tg dropdown_menu">
				<li class="tg dropdown_item">
					<span class="tg dropdown_heading">${i18n['sort-by']}</span>
					<ul class="tg dropdown_submenu">
						<li class="tg dropdown_item ${order eq 'relevance' ? '-current' : ''}">
							<a href="?query=${query}${filterQueryString}&start=${start}&limit=${limit}&order=relevance">${i18n['relevance']}</a>
						</li>
						<li class="tg dropdown_item ${order eq 'asc:title' ? '-current' : ''}">
							<a href="?query=${query}${filterQueryString}&start=${start}&limit=${limit}&order=asc:title">${i18n['title-ascending']}</a>
						</li>
						<li class="tg dropdown_item ${order eq 'desc:title' ? '-current' : ''}">
							<a href="?query=${query}${filterQueryString}&start=${start}&limit=${limit}&order=desc:title">${i18n['title-descending']}</a>
						</li>
						<li class="tg dropdown_item ${order eq 'asc:format' ? '-current' : ''}">
							<a href="?query=${query}${filterQueryString}&start=${start}&limit=${limit}&order=asc:format">${i18n['format-ascending']}</a>
						</li>
						<li class="tg dropdown_item ${order eq 'desc:format' ? '-current' : ''}">
							<a href="?query=${query}${filterQueryString}&start=${start}&limit=${limit}&order=desc:format">${i18n['format-descending']}</a>
						</li>
					</ul>
				</li>
				<li class="tg dropdown_item">
					<span class="tg dropdown_heading">${i18n['results-per-page']}</span>
					<ul class="tg dropdown_submenu">
						<li class="tg dropdown_item  ${limit eq '10' ? '-current' : ''}">
							<a href="?query=${query}${filterQueryString}&order=${order}&start=${start}&limit=10">10</a>
						</li>
						<li class="tg dropdown_item ${limit eq '20' ? '-current' : ''}">
							<a href="?query=${query}${filterQueryString}&order=${order}&start=${start}&limit=20">20</a>
						</li>
						<li class="tg dropdown_item ${limit eq '50' ? '-current' : ''}">
							<a href="?query=${query}${filterQueryString}&order=${order}&start=${start}&limit=50">50</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>

	<div class="tgrep header_actions">
		<c:if test="${mode ne 'gallery'}">
			<button class="tgrep header_button -expand-all">${i18n['expand-all']}</button>
			<button class="tgrep header_button -collapse-all">${i18n['collapse-all']}</button>
		</c:if>

		<div class="tg dropdown" role="group">
			<a class="tg dropdown_toggle -download">${i18n['download-all']}</a>
			<ul class="tg dropdown_menu">
				<li class="tg dropdown_item"><a class="tg dropdown_link" href="${aggregatorUrl}/zip/query?query=${utils:urlencode(realQueryString)}${filterQueryString}${aggregatorSandboxParam}" data-type="zip">ZIP</a></li>
<!-- other exports do not work yet
				<li class="tg dropdown_item"><a class="tg dropdown_link" href="${aggregatorUrl}/epub/query?query=${query}${filterQueryString}" data-type="ebook">E-Book</a></li>
				<li class="tg dropdown_item"><a class="tg dropdown_link" href="${aggregatorUrl}/teicorpus/query?query=${query}${filterQueryString}" data-type="tei-corpus">TEI-Corpus</a></li>
-->
			</ul>
		</div>
	</div>
</header>

<div class="tgrep results">

	<c:choose>

		<c:when test="${mode eq 'gallery'}">
			<ol start="${pager.start + 1}" class="tgrep results_gallery">
				<c:forEach items="${results}" var="result">
					<%@ include file="singleGalleryResult.jsp" %>
				</c:forEach>
			</ol>
		</c:when>

		<c:otherwise>
			<ol start="${pager.start + 1}" class="tgrep results_list">
				<c:forEach items="${results}" var="result">
					<%@ include file="singleListResult.jsp" %>
				</c:forEach>
			</ol>
		</c:otherwise>

	</c:choose>

</div>

<footer class="tgrep footer">
	<%@ include file="pager.jsp" %>
</footer>
