			<li class="tgrep gallery-item">
				<div>
					<div class="tgrep gallery-item_image">
						<a href="${utils:browseUrl(result.object.generic.generated.textgridUri.value)}">
							<img src="${utils:getImageUrl(textgridHost, result)}" alt="${result.object.generic.provided.title[0]}" title="${result.object.generic.provided.title[0]}" />
						</a>
					</div>
					<div class="tgrep gallery-item_title">
						<a href="${utils:browseUrl(result.object.generic.generated.textgridUri.value)}">
							${result.object.generic.provided.title[0]}
						</a>
					</div>
				</div>
				<div class="tgrep gallery-item_actions">
					<ul>
						<li>
							<c:choose>
								<c:when test="${basket.contains(result.object.generic.generated.textgridUri.value)}">
									<button
										class="tgrep gallery-item_button -remove-from-shelf"
										data-textgrid-targeturi="${result.object.generic.generated.textgridUri.value}">
										${i18n['remove-from-shelf']}
									</button>
								</c:when>
								<c:otherwise>
									<button
										class="tgrep gallery-item_button -add-to-shelf"
										data-textgrid-targeturi="${result.object.generic.generated.textgridUri.value}">
										${i18n['add-to-shelf']}
									</button>
								</c:otherwise>
							</c:choose>
						</li>
						<li>
							<a href="${textgridHost}/1.0/tgcrud-public/rest/${result.object.generic.generated.textgridUri.value}/data" class="tgrep gallery-item_button -download">${i18n['download']}</a>
						</li>
					</ul>
				</div>
			</li>
