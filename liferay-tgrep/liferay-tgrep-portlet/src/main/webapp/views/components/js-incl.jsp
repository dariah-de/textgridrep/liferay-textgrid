<script type="text/javascript">
	
	document.addEventListener("DOMContentLoaded", function(event) {

		$(document).on('click', '.-add-to-shelf', function(e) {
			var $this = $(this);
			$.ajax({
					url: "${addToBasketUrl}" ,
					type: 'POST',
					dataType:'json',
					data: {
						uri: $this.attr('data-textgrid-targeturi')
					},
					success: function(data) {
						$this.removeClass('-add-to-shelf');
						$this.addClass('-remove-from-shelf');
						$this.html('${i18n['remove-from-shelf']}');
						setBasketCount(data.basketSize);
					}
				 });
		});

		$(document).on('click', '.-remove-from-shelf', function(e) {
			var $this = $(this);
			$.ajax({
					url: "${removeFromBasketUrl}" ,
					type: 'POST',
					dataType:'json',
					data: {
						uri: $this.attr('data-textgrid-targeturi')
					},
					success: function(data) {
						$this.removeClass('-remove-from-shelf');
						$this.addClass('-add-to-shelf');
						$this.html('${i18n['add-to-shelf']}');
						setBasketCount(data.basketSize);
					}
			});
		});

	});


	/* DIGIVOY */
	
	function filteredTGCrudDataLink(id, query) {
		var link = "http://localhost:8080/xsltws/filter?id="+id+"&i=teiHeader";
		if (query) {
			link += "&"+query;
		}
		return link;
	}
	
	function sendToVoyant(toolUrl, documentUrl)
	{
		var voyantURL = "https://voyant.de.dariah.eu/voyant/";
		$.ajax({
			url: voyantURL + "trombone/",
			data: {
				input: documentUrl,
				// DIGIVOY-34: only text, no stuff like notes or speakers
				xmlContentXpath: "/*[local-name()='TEI']/*[local-name()='text']/*[local-name()='body']",
				// DIGIVOY-33: extract title information from TEI
				xmlTitleXpath: "/*[local-name()='TEI']/*[local-name()='teiHeader']/*[local-name()='fileDesc']/*[local-name()='titleStmt']/*[local-name()='title']",
				// DIGIVOY-33: extract author information from TEI
				xmlAuthorXpath: "/*[local-name()='TEI']/*[local-name()='teiHeader']/*[local-name()='fileDesc']/*[local-name()='sourceDesc']/*[local-name()='biblFull']/*[local-name()='titleStmt']/*[local-name()='author']",
				// DIGIVOY-33: extract publication time
				xmlTimeXpath: "/*[local-name()='TEI']/*[local-name()='teiHeader']/*[local-name()='fileDesc']/*[local-name()='sourceDesc']/*[local-name()='biblFull']/*[local-name()='publicationStmt']/*[local-name()='date']/attribute::*[local-name()='when']",
				timePattern: "yyyy",
				timeLocaleLanguage: "de",
				timeLocaleCountry: "DE",
			},
			dataType: 'json',
			success: function(json) {
				window.open(toolUrl+'?corpus='+json.corpusId+'&stopList=stop.de.german.txt', '_self', false);
			},
	        error: function(xhr, txt){
	            $('#searchresults').append('<li class="error">' + txt + '</li>');
	        }
		});
	}
	
	function sendToVoyant2(toolUrl, documentUrl)
	{
		var voyant2URL = "https://voyant.de.dariah.eu/voyant2/";
		$.ajax({
			url: voyant2URL + "trombone",
			data: {
				input: documentUrl,
				// DIGIVOY-34: only text, no stuff like notes or speakers
				xmlContentXpath: "/*[local-name()='TEI']/*[local-name()='text']/*[local-name()='body']",
				// DIGIVOY-33: extract title information from TEI
				xmlTitleXpath: "/*[local-name()='TEI']/*[local-name()='teiHeader']/*[local-name()='fileDesc']/*[local-name()='titleStmt']/*[local-name()='title']",
				// DIGIVOY-33: extract author information from TEI
				xmlAuthorXpath: "/*[local-name()='TEI']/*[local-name()='teiHeader']/*[local-name()='fileDesc']/*[local-name()='sourceDesc']/*[local-name()='biblFull']/*[local-name()='titleStmt']/*[local-name()='author']",
				// DIGIVOY-33: extract publication time
				xmlTimeXpath: "/*[local-name()='TEI']/*[local-name()='teiHeader']/*[local-name()='fileDesc']/*[local-name()='sourceDesc']/*[local-name()='biblFull']/*[local-name()='publicationStmt']/*[local-name()='date']/attribute::*[local-name()='when']",
				timePattern: "yyyy",
				timeLocaleLanguage: "de",
				timeLocaleCountry: "DE",
				tool: "corpus.CorpusMetadata",
				palette: "default",
			},
			dataType: 'json',
			success: function(json) {
				console.log(json);
				window.open(toolUrl+'?corpus='+json.corpus.metadata.id+'&stopList=stop.de.german.txt', '_self', false);
			},
	        error: function(xhr, txt){
	            $('#searchresults').append('<li class="error">' + txt + '</li>');
	        }
		});
	}
	
	
</script>