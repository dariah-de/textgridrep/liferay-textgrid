<ul class="tgrep breadcrumbs">
	<c:forEach items="${result.pathResponse.pathGroup}" var="pathGroup">
		<c:forEach items="${pathGroup.path}" var="path">
			<c:forEach items="${path.entry}" var="pathEntry">
				<li class="tgrep breadcrumbs_item">
					<a class="tgrep breadcrumbs_link" href="${utils:browseUrl(pathEntry.textgridUri)}">
						${pathEntry.title}
					</a>
				</li>
			</c:forEach>
		</c:forEach>
	</c:forEach>
	<li class="tgrep breadcrumbs_item">
		<a class="tgrep breadcrumbs_link" href="${utils:browseUrl(result.object.generic.generated.textgridUri.value)}">
			${result.object.generic.provided.title[0]}
		</a>
	</li>
</ul>