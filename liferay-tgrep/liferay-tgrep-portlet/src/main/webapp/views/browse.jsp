<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://textgrid.info/repportlet/utils" prefix="utils" %>

<portlet:defineObjects />

<portlet:resourceURL id="addToBasket" var="addToBasketUrl" ></portlet:resourceURL>
<portlet:resourceURL id="removeFromBasket" var="removeFromBasketUrl" ></portlet:resourceURL>

<%@ include file="components/js-incl.jsp" %>

<style>
iframe {
  width: 100%;
}

</style>

<script type="text/javascript">

document.addEventListener("DOMContentLoaded", function(event) {
	$("#htmlIframe").contents().find("body").attr("style","margin-left: 0");
});

</script>

<c:if test="${annotationEnabled}">
<link rel="stylesheet" type="text/css" href="https://annotation.de.dariah.eu/annotatorjs/annotator.min.css" />
<script type="text/javascript" src="https://annotation.de.dariah.eu/annotatorjs/annotator-full.min.js"></script>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
  var token = '${annotationToken}';
  console.log("tkoen: " + token);
  var ann = new Annotator(document.body);
  /*ann.addPlugin('Auth', {
    'token': token
  });*/
  ann.addPlugin('GroupPermissions');
  var annotatorServerUrl = 'https://annotation.de.dariah.eu/AnnotationManager';
  var uri = window.location.href;

  ann.addPlugin('Store', {
      prefix: annotatorServerUrl+'/annotator',
      annotationData : {
          'uri' : uri,
      },
      loadFromSearch : {
          'limit' : 20,
          'uri' : uri
      }
  });

  // tags
  ann.addPlugin('Tags');
  ann.addPlugin('Filter');
});
</script>

</c:if>


<div class="tgrep wrap">

	<c:if test="${browseRootAggregations}">
		<aside class="tgrep sidebar">
			<c:if test="${viewmodes != null}">
				<section class="tgrep sidebar_panel">
					<h3 class="tgrep sidebar_subheading">${i18n['views']}</h3>
					<ul class="tgrep sidebar_list">
						<c:forEach items="${viewmodes}" var="viewmode">
							<li class="tgrep sidebar_item ${viewmode.active? '-current' : ''}">
								<a href="${viewmode.url}" rel="noindex nofollow" class="tgrep sidebar_link">${viewmode.label}</a>
							</li>
						</c:forEach>
					</ul>
					<c:if test="${viewmodes.size() > 6}">
						<button class="tgrep sidebar_expand">${i18n['expand']}</button>
					</c:if>
				</section>
			</c:if>
		</aside>
	</c:if>

	<c:if test="${! browseRootAggregations}">
		<div class="tgrep sidebar-toggle">
			<button class="tgrep sidebar-toggle_button -show">${i18n['show-sidebar']}</button>
			<button class="tgrep sidebar-toggle_button -hide">${i18n['hide-sidebar']}</button>
		</div>

		<aside class="tgrep sidebar">

	<!--
			<h:panelGroup rendered="${browseBean.id != null}" layout="block" class="egal">
				<strong>Metadata</strong><br/>
	  	   		<a href="https://textgridlab.org/1.0/tgcrud-public/rest/${browseBean.metadata.object.generic.generated.textgridUri.value}/metadata">raw metadata</a>
				<hr/>
				<strong>Tools</strong><br/>
				<a href="#">Voyant</a><br/>
				<a href="#">Digilib</a><br/>
				<a href="#">Mirador</a><br/>
				<a href="#">DFG-Viewer</a><br/>
			</h:panelGroup>
	-->

			<section class="tgrep sidebar_panel">
				<h3 class="tgrep sidebar_subheading">${i18n['metadata']}</h3>
				<!-- TODO: each for agent, pid, etc -->
				<dl>
					<dt>${i18n['format']}</dt><dd>${metadata.object.generic.provided.format}</dd>

					<!-- show author when available, first agent otherwise -->
					<c:choose>
						<c:when test="${not empty metadata.object.edition.source[0].bibliographicCitation.author[0].value}">
							<dt>${i18n['author']}</dt>
							<dd>
								${metadata.object.edition.source[0].bibliographicCitation.author[0].value}
								<c:if test="${not empty metadata.object.edition.source[0].bibliographicCitation.author[0].id}">
									(<a href="http://d-nb.info/gnd/${fn:substringAfter(metadata.object.edition.source[0].bibliographicCitation.author[0].id, 'pnd:')}">GND</a>)
								</c:if>
							</dd>
						</c:when>
						<c:when test="${not empty metadata.object.edition.agent[0].value}">
							<dt>${i18n['agent']}</dt>
							<c:forEach items="${metadata.object.edition.agent}" var="agent">
								<dd>
									${agent.value}
									<c:if test="${not empty agent.role}">
										(${fn:substring(agent.role, 0, 1)}${fn:toLowerCase(fn:substring(agent.role, 1, -1))})
									</c:if>
									<c:if test="${not empty agent.id}">
										<a href="http://d-nb.info/gnd/${fn:substringAfter(agent.id, 'pnd:')}">GND</a>
									</c:if>
								</dd>
							</c:forEach>
						</c:when>
					</c:choose>

					<c:if test="${not empty metadata.object.edition.source[0].bibliographicCitation.dateOfPublication.date}">
						<dt>${i18n['date-of-publication']}</dt><dd>${metadata.object.edition.source[0].bibliographicCitation.dateOfPublication.date}</dd>
					</c:if>
					<c:if test="${not empty metadata.object.edition.source[0].bibliographicCitation.placeOfPublication[0].value}">
						<dt>${i18n['place-of-publication']}</dt><dd>${metadata.object.edition.source[0].bibliographicCitation.placeOfPublication[0].value}</dd>
					</c:if>
					<c:if test="${not empty metadata.object.generic.generated.pid[0].value}">
						<dt>${i18n['pid']}</dt>
                        <dd><a href="https://hdl.handle.net/${fn:substringAfter(metadata.object.generic.generated.pid[0].value, 'hdl:')}">${metadata.object.generic.generated.pid[0].value}</a><br/>
                        <a href="#citation">${i18n['citation']}</a></dd>
					</c:if>
				</dl>
			</section>

			<section class="tgrep sidebar_panel">
				<h3 class="tgrep sidebar_subheading">${i18n['download']}</h3>
				<ul class="tgrep sidebar_list">
					<li>
						<a href="${textgridHost}/1.0/tgcrud-public/rest/${metadata.object.generic.generated.textgridUri.value}/data">
							${i18n['object']} <c:if test="${isTEI}">(TEI)</c:if>
						</a>
					</li>
                    <li>
						<a href="${textgridHost}/1.0/tgcrud-public/rest/${metadata.object.generic.generated.textgridUri.value}/metadata">
							${i18n['metadata']} (XML)
						</a>
					</li>
                    <li>
						<a href="${textgridHost}/1.0/tgcrud-public/rest/${metadata.object.generic.generated.textgridUri.value}/tech">
							${i18n['techmd']} (XML)
						</a>
					</li>
					<c:if test="${isTEI}">
    					<li>
                            <a href="${textgridHost}/1.0/aggregator/text/${metadata.object.generic.generated.textgridUri.value}">
                                Plain Text (txt)
                            </a>
                        </li>
                        <li>
							<a href="${textgridHost}/1.0/aggregator/epub/${metadata.object.generic.generated.textgridUri.value}">
							  E-Book (epub)
							</a>
						</li>
						<li>
							<a href="${textgridHost}/1.0/aggregator/html/${metadata.object.generic.generated.textgridUri.value}">
							  HTML
							</a>
						</li>
						<li>
							<a href="${textgridHost}/1.0/aggregator/zip/${metadata.object.generic.generated.textgridUri.value}">
							  ZIP
							</a>
						</li>
					</c:if>
					<c:if test="${fn:contains(metadata.object.generic.provided.format, 'aggregation')}">
						<a href="${textgridHost}/1.0/aggregator/teicorpus/${metadata.object.generic.generated.textgridUri.value}">
						  TEI-Corpus (XML)
						</a>
					</c:if>
				</ul>
			</section>

			<c:if test="${viewmodes != null}">
				<section class="tgrep sidebar_panel">
					<h3 class="tgrep sidebar_subheading">${i18n['views']}</h3>
					<ul class="tgrep sidebar_list">
						<c:forEach items="${viewmodes}" var="viewmode">
							<li class="tgrep sidebar_item ${viewmode.active? '-current' : ''}">
								<a href="${viewmode.url}" rel="noindex nofollow" class="tgrep sidebar_link">${viewmode.label}</a>
							</li>
						</c:forEach>
					</ul>
					<c:if test="${viewmodes.size() > 6}">
						<button class="tgrep sidebar_expand">${i18n['expand']}</button>
					</c:if>
				</section>
			</c:if>

			<c:if test="${tools != null and tools.size() > 0}">
				<section class="tgrep sidebar_panel">
					<h3 class="tgrep sidebar_subheading">${i18n['tools']}</h3>
					<ul class="tgrep sidebar_list">
						<c:forEach items="${tools}" var="tool">
							<li class="tgrep sidebar_item">
								<a href="${tool.url}" rel="noindex nofollow" class="tgrep sidebar_link ${tool.cssClass}">${tool.label}</a>
							</li>
						</c:forEach>
					</ul>
					<c:if test="${tools.size() > 6}">
						<button class="tgrep sidebar_expand">${i18n['expand']}</button>
					</c:if>
				</section>
			</c:if>

			<c:if test="${tocHtml != null and metadata.object.generic.generated.project.value eq 'Digitale Bibliothek'}">

			<section class="tgrep sidebar_panel">
				<h3 class="tgrep sidebar_subheading">${i18n['toc']}</h3>
					${tocHtml}
			</section>
			</c:if>

		</aside>
	</c:if>

	<main class="tgrep main">
<!--
		<h1 class="tgrep main_heading">Browse</h1>
-->
		<%@ include file="components/path.jsp" %>

		<c:choose>
			<c:when test="${results != null}">
				<c:if test="${format == 'text/tg.work+xml'}">
					<h1 class="tgrep main_heading">${i18n['editions-for-this-work']}</h1>
				</c:if>

				<div class="tgrep results">
					<c:choose>
						<c:when test="${mode eq 'gallery'}">
							<ol class="tgrep results_gallery">
								<c:forEach items="${results}" var="result">
									<%@ include file="components/singleGalleryResult.jsp" %>
								</c:forEach>
							</ol>
						</c:when>

						<c:otherwise>
							<ol class="tgrep results_list">
								<c:forEach items="${results}" var="result">
									<%@ include file="components/singleListResult.jsp" %>
								</c:forEach>
							</ol>
						</c:otherwise>
					</c:choose>
				</div>
			</c:when>

			<c:when test="${teiHtml != null}">
				<div class="teiXsltView">
					${teiHtml}
				</div>
			</c:when>

			<c:when test="${image}">
				<img src="${textgridHost}/1.0/digilib/rest/IIIF/${metadata.object.generic.generated.textgridUri.value}/full/,1500/0/native.jpg"/>
			</c:when>

			<c:when test="${metadata.object.generic.provided.identifier[0].type eq 'METSXMLID'}">
				${i18n['dfg-viewer-mets-message']}
			</c:when>

			<c:when test="${metadata.object.generic.provided.format eq 'text/html'}">
				<p>
					${i18n['iframe-html-message']}
				</p>
				<a target="_blank" href="${textgridHost}/1.0/tgcrud-public/rest/${metadata.object.generic.generated.textgridUri.value}/data">${i18n['open-html-in-new-window']}</a>
				<iframe id="htmlIframe" width="560" height="600" frameborder="0" src="${textgridHost}/1.0/tgcrud-public/rest/${metadata.object.generic.generated.textgridUri.value}/data"></iframe>
			</c:when>

			<c:otherwise>
				<p>${i18n['dont-know-what-todo']} ${format}</p>
				<a target="_blank" href="${textgridHost}/1.0/tgcrud-public/rest/${metadata.object.generic.generated.textgridUri.value}/data">${i18n['open-html-in-new-window']}</a>
			</c:otherwise>

		</c:choose>

		<!-- Notes and License (e.g. for editions) -->
		<c:if test="${not empty metadata.object.edition.license.value or not empty metadata.object.generic.provided.notes}">
			<div style="clear:both">
			<hr/>
			<dl>
				<c:if test="${not empty metadata.object.generic.provided.notes}">
					<dt>${i18n['notes']}</dt><dd>${metadata.object.generic.provided.notes}</dd>
				</c:if>
				<c:if test="${not empty metadata.object.item.rightsHolder[0].value}">
					<dt>${i18n['rightsholder']}</dt><dd>${metadata.object.item.rightsHolder[0].value}</dd>
				</c:if>
				<c:if test="${not empty metadata.object.edition.license.value}">
					<dt>${i18n['license']}</dt>
						<dd>
							${metadata.object.edition.license.value}
							<c:if test="${not empty metadata.object.edition.license.licenseUri}">
								<br/><a href="${metadata.object.edition.license.licenseUri}">${i18n['licenseUri']}</a>
							</c:if>
						</dd>
				</c:if>
			</dl>
			</div>
		</c:if>

        <!-- Citation examples -->
        <div id="citation" style="clear:both">
        <hr/>
        <%@ include file="components/citation.jsp" %>

	</main>

</div>
