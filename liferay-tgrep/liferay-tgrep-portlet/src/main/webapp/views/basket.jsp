<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://textgrid.info/repportlet/utils" prefix="utils" %>

<portlet:defineObjects />

<portlet:resourceURL id="removeFromBasket" var="removeFromBasketUrl" ></portlet:resourceURL>
<portlet:resourceURL id="clearBasket" var="clearBasketUrl" ></portlet:resourceURL>

<%@ include file="components/js-incl.jsp" %>

<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function(event) {

		$(document).on('click', '.-remove-from-shelf', function(e) {
			var $this = $(this);

			$.ajax({
					url: "${removeFromBasketUrl}" ,
					type: 'POST',
					dataType:'json',
					data: {
						uri: $this.attr('data-textgrid-targeturi')
					},
					success: function(data) {
						$this.closest('li.tgrep.result').remove();
						$('.tgrep.header_count').html(data.basketSize);
						setBasketCount(data.basketSize); // this call needs the basketcount portlet available
					}
			});
		});

		$(document).on('click', '.tgrep.header_button.-clear', function(e) {
			var $this = $(this);

			$.ajax({
					url: "${clearBasketUrl}" ,
					type: 'POST',
					dataType:'json',
					data: {
						empty: "empty"
					},
					success: function(data) {
						$('.tgrep.results_list').empty();
						$('.tgrep.header_count').html(0);
						setBasketCount(0); // this call needs the basketcount portlet available
					}
			});
		});




	/*
	 *  VOYANT
	 *
	 */

		//var voyantURL = "https://textgridrep.de/voyant/"
	
		$(document).on('click', '.digivoy-basket-button', function(e) {
	          var toolUrl = 'https://voyant.de.dariah.eu/voyant';
			  var documentLinks = [];
			  var settings_serialized = "target="+encodeURIComponent('${textgridHost}/voyant')+"%2F&e=castList&e=desc&e=figure&e=head&e=note&e=speaker&e=stage&e=title";
			  
			  $('.tgrep.result_main').each(function() {
				  var documentId = unescape($(this).attr('data-tg-uri'));
				  documentLinks.push(filteredTGCrudDataLink(documentId, settings_serialized));
			  });
			  sendToVoyant(toolUrl, documentLinks.join('\n'));
	    });
	
		$(document).on('click', '.digivoy2-basket-button', function(e) {
	          var toolUrl = 'https://voyant.de.dariah.eu/voyant2/';
	          //var toolUrl = 'http://voyant.de.dariah.eu/voyant2';
			  var documentLinks = [];
			  var settings_serialized = "target="+encodeURIComponent('${textgridHost}/voyant2')+"%2F&e=castList&e=desc&e=figure&e=head&e=note&e=speaker&e=stage&e=title";
			  
			  $('.tgrep.result_main').each(function() {
				  var documentId = unescape($(this).attr('data-tg-uri'));
				  documentLinks.push(filteredTGCrudDataLink(documentId, settings_serialized));
			  });
			  sendToVoyant2(toolUrl, documentLinks.join('\n'));
	    });
	
		$(document).on('click', '.collate-button', function(e) {
	
		});
	
	});
</script>

<div class="tgrep wrap">
	<div class="tgrep sidebar-toggle">
		<button class="tgrep sidebar-toggle_button -show">${i18n['show-sidebar']}</button>
		<button class="tgrep sidebar-toggle_button -hide">${i18n['hide-sidebar']}</button>
	</div>

	<aside class="tgrep sidebar">
<!--
		<section class="tgrep sidebar_panel">
			<h3 class="tgrep sidebar_subheading">${i18n['views']}<h3>
			<ul class="tgrep sidebar_list">
				<li class="tgrep sidebar_item">
					<a class="tgrep sidebar_link">XML</a>
				</li>
				<li class="tgrep sidebar_item">
					<a class="tgrep sidebar_link">Browse</a>
				</li>
				<li class="tgrep sidebar_item">
					<a class="tgrep sidebar_link">Gallery</a>
				</li>
				<li class="tgrep sidebar_item">
					<a class="tgrep sidebar_link">Digilib</a>
				</li>
			</ul>
		</section>

		<section class="tgrep sidebar_panel">
			<h3 class="tgrep sidebar_subheading">${i18n['tools']}</h3>
			<ul class="tgrep sidebar_list">
				<li class="tgrep sidebar_item">
					<a class="tgrep sidebar_link voyant-button">Digivoy</a>
				</li>
				<li class="tgrep sidebar_item">
					<a class="tgrep sidebar_link collate-button">CollateX</a>
				</li>
			</ul>
		</section>
-->

		<c:if test="${viewmodes != null}">
			<section class="tgrep sidebar_panel">
				<h3 class="tgrep sidebar_subheading">${i18n['views']}</h3>
				<ul class="tgrep sidebar_list">
					<c:forEach items="${viewmodes}" var="viewmode">
						<li class="tgrep sidebar_item">
							<a href="${viewmode.url}" rel="noindex nofollow" class="tgrep sidebar_link">${viewmode.label}</a>
						</li>
					</c:forEach>
				</ul>
				<c:if test="${viewmodes.size() > 6}">
					<button class="tgrep sidebar_expand">${i18n['expand']}</button>
				</c:if>
			</section>
		</c:if>

		<c:if test="${tools != null and tools.size() > 0}">
			<section class="tgrep sidebar_panel">
				<h3 class="tgrep sidebar_subheading">${i18n['tools']}</h3>
				<ul class="tgrep sidebar_list">
					<c:forEach items="${tools}" var="tool">
						<li class="tgrep sidebar_item">
							<a href="${tool.url}" rel="noindex nofollow" class="tgrep sidebar_link ${tool.cssClass}">${tool.label}</a>
						</li>
					</c:forEach>
				</ul>
				<c:if test="${tools.size() > 6}">
					<button class="tgrep sidebar_expand">${i18n['expand']}</button>
				</c:if>
			</section>
		</c:if>
	</aside>

	<main class="tgrep main">
		<h1 class="tgrep main_heading">${i18n['shelf']}</h1>

		<header class="tgrep header">
			<h2 class="tgrep header_heading">${i18n['manage-the-shelf']}</h2>
			<div class="tgrep header_info">
				${i18n['there-are-currently']} <span class="tgrep header_count">${results.size()}</span> ${i18n['objects-on-your-shelf']}
			</div>
			<div class="tgrep header_actions">
				<button class="tgrep header_button -expand-all">${i18n['expand-all']}</button>
				<button class="tgrep header_button -collapse-all">${i18n['collapse-all']}</button>
				<button class="tgrep header_button -clear">${i18n['clear']}</button>
				<div class="tg dropdown" role="group">
					<a class="tg dropdown_toggle">${i18n['download-all']}</a>
					<ul class="tg dropdown_menu">
						<li class="tg dropdown_item"><a class="tg dropdown_link" href="${aggregatorUrl}/epub/${basketItemString}" data-type="ebook">E-Book</a></li>
						<li class="tg dropdown_item"><a class="tg dropdown_link" href="${aggregatorUrl}/zip/${basketItemString}" data-type="zip">ZIP</a></li>
						<li class="tg dropdown_item"><a class="tg dropdown_link" href="${aggregatorUrl}/teicorpus/${basketItemString}" data-type="teicorpus">TEI-Corpus</a></li>
					</ul>
				</div>
			</div>
		</header>

		<c:choose>
			<c:when test="${mode eq 'gallery'}">
				<ul class="tgrep results_gallery">
					<c:forEach items="${results}" var="result">
						<%@ include file="components/singleGalleryResult.jsp" %>
					</c:forEach>
				</ul>
			</c:when>
			<c:otherwise>
				<ul class="tgrep results_list">
					<c:forEach items="${results}" var="result">
						<%@ include file="components/singleListResult.jsp" %>
					</c:forEach>
				</ul>
			</c:otherwise>
		</c:choose>
	</main>
</div>
