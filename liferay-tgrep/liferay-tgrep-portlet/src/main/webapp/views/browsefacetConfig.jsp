<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://textgrid.info/repportlet/utils" prefix="utils" %>

<portlet:defineObjects />

<portlet:actionURL var="submitFormURL" name="submitFormURL"/>
<portlet:actionURL var="resetFormURL" name="resetFormURL"/>

<h1>Config!!!</h1>

<form method="POST" action="${submitFormURL}">

		<table>
			<tr>
				<td><label for="facet">Facette</label></td>
				<td><input name="facet" value="${facet}" /></td>
			</tr>
			<tr>
				<td><label for="limit">limit</label></td>
				<td><input name="limit" value="${limit}" /></td>
			</tr>
			<tr>
				<td><label for="order">order</label></td>
				<td><input name="order" value="${order}" /></td>
				<td><em>e.g. count:desc, term:asc</em></td>
			</tr>
		</table>
		
		<button>submit</button>
</form>

<form method="POST" action="${resetFormURL}">		
	<button>reset</button>	
</form>

	

